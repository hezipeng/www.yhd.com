package com.item.yhd.controller;

import com.item.yhd.po.YhdMerchandiseLabel;
import com.item.yhd.service.YhdMerchandiseLabelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/yhdMerchandiseLabelController")
public class YhdMerchandiseLabelController {

    @Autowired
    private YhdMerchandiseLabelService yhdMerchandiseLabelService;


    @RequestMapping(value = "/getShowOneYhdMerchandiseLabel/{merchandiseLabelId}", method = RequestMethod.GET)
    public YhdMerchandiseLabel getShowOneYhdMerchandiseLabel(@PathVariable("merchandiseLabelId") int merchandiseLabelId) {
        return this.yhdMerchandiseLabelService.getOneYhdMerchandiseLabelService(merchandiseLabelId);
    }

    @RequestMapping(value = "/addInsertYhdMerchandiseLabel", method = RequestMethod.GET)
    public YhdMerchandiseLabel addInsertYhdMerchandiseLabel(YhdMerchandiseLabel yhdMerchandiseLabel) {
        return this.yhdMerchandiseLabelService.addYhdMerchandiseLabelService(yhdMerchandiseLabel);
    }

    @RequestMapping(value = "/setUpdateYhdMerchandiseLabel", method = RequestMethod.GET)
    public YhdMerchandiseLabel setUpdateYhdMerchandiseLabel(YhdMerchandiseLabel yhdMerchandiseLabel) {
        return this.yhdMerchandiseLabelService.setYhdMerchandiseLabelService(yhdMerchandiseLabel);
    }

    @RequestMapping(value = "/delDeleteYhdMerchandiseLabel/{merchandiseLabelId}", method = RequestMethod.GET)
    public boolean delDeleteYhdMerchandiseLabel(@PathVariable("merchandiseLabelId") int merchandiseLabelId) {
        return this.yhdMerchandiseLabelService.delYhdMerchandiseLabelService(merchandiseLabelId);
    }

}
