package com.item.yhd.controller;

import com.item.yhd.po.YhdMerchandiseLargeType;
import com.item.yhd.service.YhdMerchandiseLargeTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/yhdMerchandiseLargeTypeController")
public class YhdMerchandiseLargeTypeController {

    @Autowired
    private YhdMerchandiseLargeTypeService yhdMerchandiseLargeTypeService;


    @RequestMapping(value = "/getShowListYhdMerchandiseLargeType", method = RequestMethod.GET)
    public List<YhdMerchandiseLargeType> getShowListYhdMerchandiseLargeType() {
        return this.yhdMerchandiseLargeTypeService.getListYhdMerchandiseLargeTypeService();
    }

    @RequestMapping(value = "/getShowOneYhdMerchandiseLargeType/{largeTypeId}", method = RequestMethod.GET)
    public YhdMerchandiseLargeType getShowOneYhdMerchandiseLargeType(@PathVariable("largeTypeId") int largeTypeId) {
        return this.yhdMerchandiseLargeTypeService.getOneYhdMerchandiseLargeTypeService(largeTypeId);
    }

    @RequestMapping(value = "/addInsertYhdMerchandiseLargeType")
    public YhdMerchandiseLargeType addInsertYhdMerchandiseLargeType(YhdMerchandiseLargeType yhdMerchandiseLargeType) {
        return this.yhdMerchandiseLargeTypeService.addYhdMerchandiseLargeTypeService(yhdMerchandiseLargeType);
    }

    @RequestMapping(value = "/setUpdateYhdMerchandiseLargeType")
    public YhdMerchandiseLargeType setUpdateYhdMerchandiseLargeType(YhdMerchandiseLargeType yhdMerchandiseLargeType) {
        return this.yhdMerchandiseLargeTypeService.setYhdMerchandiseLargeTypeService(yhdMerchandiseLargeType);
    }

    @RequestMapping(value = "/delDeleteYhdMerchandiseLargeType/{largeTypeId}", method = RequestMethod.GET)
    public boolean delDeleteYhdMerchandiseLargeType(@PathVariable("largeTypeId") int largeTypeId) {
        return this.yhdMerchandiseLargeTypeService.delYhdMerchandiseLargeTypeService(largeTypeId);
    }

}
