package com.item.yhd.controller;

import com.item.yhd.po.YhdMerchandiseSmallType;
import com.item.yhd.service.YhdMerchandiseSmallTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/yhdMerchandiseSmallTypeController")
public class YhdMerchandiseSmallTypeController {

    @Autowired
    private YhdMerchandiseSmallTypeService yhdMerchandiseSmallTypeService;


    @RequestMapping(value = "/getShowListLargeTypeIdYhdMerchandiseSmallType/{largeTypeId}", method = RequestMethod.GET)
    public List<YhdMerchandiseSmallType> getShowListLargeTypeIdYhdMerchandiseSmallType(@PathVariable("largeTypeId") int largeTypeId) {
        return this.yhdMerchandiseSmallTypeService.getListLargeTypeIdYhdMerchandiseSmallTypeService(largeTypeId);
    }

    @RequestMapping(value = "/addInsertYhdMerchandiseSmallType")
    public YhdMerchandiseSmallType addInsertYhdMerchandiseSmallType(YhdMerchandiseSmallType yhdMerchandiseSmallType) {
        return this.yhdMerchandiseSmallTypeService.addYhdMerchandiseSmallTypeService(yhdMerchandiseSmallType);
    }

    @RequestMapping(value = "/setUpdateYhdMerchandiseSmallType")
    public YhdMerchandiseSmallType setUpdateYhdMerchandiseSmallType(YhdMerchandiseSmallType yhdMerchandiseSmallType) {
        return this.yhdMerchandiseSmallTypeService.setYhdMerchandiseSmallTypeService(yhdMerchandiseSmallType);
    }

    @RequestMapping(value = "/delDeleteYhdMerchandiseSmallType/{smallTypeId}", method = RequestMethod.GET)
    public boolean delDeleteYhdMerchandiseSmallType(@PathVariable("smallTypeId") int smallTypeId) {
        return this.yhdMerchandiseSmallTypeService.delYhdMerchandiseSmallTypeService(smallTypeId);
    }

}
