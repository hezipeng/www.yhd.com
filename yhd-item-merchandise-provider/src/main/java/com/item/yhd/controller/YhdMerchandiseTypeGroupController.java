package com.item.yhd.controller;

import com.item.yhd.po.YhdMerchandiseTypeGroup;
import com.item.yhd.service.YhdMerchandiseTypeGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/yhdMerchandiseTypeGroupController")
public class YhdMerchandiseTypeGroupController {

    @Autowired
    private YhdMerchandiseTypeGroupService yhdMerchandiseTypeGroupService;


    @RequestMapping(value = "/getShowListYhdMerchandiseTypeGroup", method = RequestMethod.GET)
    public List<YhdMerchandiseTypeGroup> getShowListYhdMerchandiseTypeGroup() {
        return this.yhdMerchandiseTypeGroupService.getListYhdMerchandiseTypeGroupService();
    }

    @RequestMapping(value = "/addInsertYhdMerchandiseTypeGroup")
    public YhdMerchandiseTypeGroup addInsertYhdMerchandiseTypeGroup(YhdMerchandiseTypeGroup yhdMerchandiseTypeGroup) {
        return this.yhdMerchandiseTypeGroupService.addYhdMerchandiseTypeGroupService(yhdMerchandiseTypeGroup);
    }

    @RequestMapping(value = "/setUpdateYhdMerchandiseTypeGroup")
    public YhdMerchandiseTypeGroup setUpdateYhdMerchandiseTypeGroup(YhdMerchandiseTypeGroup yhdMerchandiseTypeGroup) {
        return this.yhdMerchandiseTypeGroupService.setYhdMerchandiseTypeGroupService(yhdMerchandiseTypeGroup);
    }

    @RequestMapping(value = "/delDeleteYhdMerchandiseTypeGroup/{groupId}", method = RequestMethod.GET)
    public boolean delDeleteYhdMerchandiseTypeGroup(@PathVariable("groupId") int groupId) {
        return this.yhdMerchandiseTypeGroupService.delYhdMerchandiseTypeGroupService(groupId);
    }

}
