package com.item.yhd.controller;

import com.item.yhd.po.YhdMerchandiseSmallDetailed;
import com.item.yhd.service.YhdMerchandiseSmallDetailedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/yhdMerchandiseSmallDetailedController")
public class YhdMerchandiseSmallDetailedController {

    @Autowired
    private YhdMerchandiseSmallDetailedService yhdMerchandiseSmallDetailedService;


    @RequestMapping(value = "/getShowListLargeDetailedIdYhdMerchandiseSmallDetailed/{largeDetailedId}", method = RequestMethod.GET)
    public List<YhdMerchandiseSmallDetailed> getShowListLargeDetailedIdYhdMerchandiseSmallDetailed(@PathVariable("largeDetailedId") int largeDetailedId) {
        return this.yhdMerchandiseSmallDetailedService.getListLargeDetailedIdYhdMerchandiseSmallDetailedService(largeDetailedId);
    }

    @RequestMapping(value = "/getOneYhdMerchandiseSmallDetailed/{smallDetailedId}", method = RequestMethod.GET)
    public YhdMerchandiseSmallDetailed getOneYhdMerchandiseSmallDetailed(@PathVariable("smallDetailedId") int smallDetailedId) {
        return this.yhdMerchandiseSmallDetailedService.getOneYhdMerchandiseSmallDetailedService(smallDetailedId);
    }

    @RequestMapping(value = "/addInsertYhdMerchandiseSmallDetailed")
    public YhdMerchandiseSmallDetailed addInsertYhdMerchandiseSmallDetailed(YhdMerchandiseSmallDetailed yhdMerchandiseSmallDetailed) {
        return this.yhdMerchandiseSmallDetailedService.addYhdMerchandiseSmallDetailedService(yhdMerchandiseSmallDetailed);
    }

    @RequestMapping(value = "/setUpdateYhdMerchandiseSmallDetailed")
    public YhdMerchandiseSmallDetailed setUpdateYhdMerchandiseSmallDetailed(YhdMerchandiseSmallDetailed yhdMerchandiseSmallDetailed) {
        return this.yhdMerchandiseSmallDetailedService.setYhdMerchandiseSmallDetailedService(yhdMerchandiseSmallDetailed);
    }

    @RequestMapping(value = "/delDeleteYhdMerchandiseSmallDetailed/{smallDetailedId}", method = RequestMethod.GET)
    public boolean delDeleteYhdMerchandiseSmallDetailed(@PathVariable("smallDetailedId") int smallDetailedId) {
        return this.yhdMerchandiseSmallDetailedService.delYhdMerchandiseSmallDetailedService(smallDetailedId);
    }

}
