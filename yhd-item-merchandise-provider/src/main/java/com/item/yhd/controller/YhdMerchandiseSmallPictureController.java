package com.item.yhd.controller;

import com.item.yhd.po.YhdMerchandiseSmallPicture;
import com.item.yhd.service.YhdMerchandiseSmallPictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/yhdMerchandiseSmallPictureController")
public class YhdMerchandiseSmallPictureController {

    @Autowired
    private YhdMerchandiseSmallPictureService yhdMerchandiseSmallPictureService;


    @RequestMapping(value = "/getShowListSmallDetailedIdYhdMerchandiseSmallPicture/{smallDetailedId}", method = RequestMethod.GET)
    public List<YhdMerchandiseSmallPicture> getShowListSmallDetailedIdYhdMerchandiseSmallPicture(@PathVariable("smallDetailedId") int smallDetailedId) {
        return this.yhdMerchandiseSmallPictureService.getListSmallDetailedIdYhdMerchandiseSmallPictureService(smallDetailedId);
    }

    @RequestMapping(value = "/addInsertYhdMerchandiseSmallPicture")
    public YhdMerchandiseSmallPicture addInsertYhdMerchandiseSmallPicture(YhdMerchandiseSmallPicture yhdMerchandiseSmallPicture) {
        return this.yhdMerchandiseSmallPictureService.addYhdMerchandiseSmallPictureService(yhdMerchandiseSmallPicture);
    }

    @RequestMapping(value = "/setUpdateYhdMerchandiseSmallPicture")
    public YhdMerchandiseSmallPicture setUpdateYhdMerchandiseSmallPicture(YhdMerchandiseSmallPicture yhdMerchandiseSmallPicture) {
        return this.yhdMerchandiseSmallPictureService.setYhdMerchandiseSmallPictureService(yhdMerchandiseSmallPicture);
    }

    @RequestMapping(value = "/delDeleteYhdMerchandiseSmallPicture/{pictureId}", method = RequestMethod.GET)
    public boolean delDeleteYhdMerchandiseSmallPicture(@PathVariable("pictureId") int pictureId) {
        return this.yhdMerchandiseSmallPictureService.delYhdMerchandiseSmallPictureService(pictureId);
    }

}
