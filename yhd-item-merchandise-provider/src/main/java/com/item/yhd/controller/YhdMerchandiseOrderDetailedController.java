package com.item.yhd.controller;

import com.item.yhd.po.YhdMerchandiseOrderDetailed;
import com.item.yhd.service.YhdMerchandiseOrderDetailedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/yhdMerchandiseOrderDetailedController")
public class YhdMerchandiseOrderDetailedController {

    @Autowired
    private YhdMerchandiseOrderDetailedService yhdMerchandiseOrderDetailedService;


    @RequestMapping(value = "/getShowListYhdMerchandiseOrderDetailed/{orderId}", method = RequestMethod.GET)
    public List<YhdMerchandiseOrderDetailed> getShowListYhdMerchandiseOrderDetailed(@PathVariable("orderId") String orderId) {
        return this.yhdMerchandiseOrderDetailedService.getListYhdMerchandiseOrderDetailedService(orderId);
    }

    @RequestMapping(value = "/addInsertYhdMerchandiseOrderDetailed")
    public YhdMerchandiseOrderDetailed addInsertYhdMerchandiseOrderDetailed(YhdMerchandiseOrderDetailed yhdMerchandiseOrderDetailed) {
        return this.yhdMerchandiseOrderDetailedService.addYhdMerchandiseOrderDetailedService(yhdMerchandiseOrderDetailed);
    }

    @RequestMapping(value = "/setUpdateYhdMerchandiseOrderDetailed")
    public YhdMerchandiseOrderDetailed setUpdateYhdMerchandiseOrderDetailed(YhdMerchandiseOrderDetailed yhdMerchandiseOrderDetailed) {
        return this.yhdMerchandiseOrderDetailedService.setYhdMerchandiseOrderDetailedService(yhdMerchandiseOrderDetailed);
    }

    @RequestMapping(value = "/delDeleteYhdMerchandiseOrderDetailed/{orderId}/{largeDetailedId}/{smallDetailedId}")
    public boolean delDeleteYhdMerchandiseOrderDetailed(@PathVariable("orderId") String orderId, @PathVariable("largeDetailedId") int largeDetailedId, @PathVariable("smallDetailedId") int smallDetailedId) {
        return this.yhdMerchandiseOrderDetailedService.delYhdMerchandiseOrderDetailedService(orderId, largeDetailedId, smallDetailedId);
    }

}
