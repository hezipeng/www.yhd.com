package com.item.yhd.controller;

import com.item.yhd.po.YhdMerchandiseOrder;
import com.item.yhd.service.YhdMerchandiseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/yhdMerchandiseOrderController")
public class YhdMerchandiseOrderController {

    @Autowired
    private YhdMerchandiseOrderService yhdMerchandiseOrderService;


    @RequestMapping(value = "/getShowPageYhdMerchandiseOrder/{page}/{size}/{userId}", method = RequestMethod.GET)
    public Map<String, Object> getShowPageYhdMerchandiseOrder(@PathVariable("page") int page, @PathVariable("size") int size, @PathVariable("userId") int userId) {
        return this.yhdMerchandiseOrderService.getPageYhdMerchandiseOrderService(page, size, userId);
    }

    @RequestMapping(value = "/getOneYhdMerchandiseOrder/{orderId}", method = RequestMethod.GET)
    public YhdMerchandiseOrder getOneYhdMerchandiseOrder(@PathVariable("orderId") String orderId) {
        return this.yhdMerchandiseOrderService.getOneYhdMerchandiseOrderService(orderId);
    }

    @RequestMapping(value = "/addInsertYhdMerchandiseOrder")
    public YhdMerchandiseOrder addInsertYhdMerchandiseOrder(YhdMerchandiseOrder yhdMerchandiseOrder) {
        return this.yhdMerchandiseOrderService.addYhdMerchandiseOrderService(yhdMerchandiseOrder);
    }

    @RequestMapping(value = "/setUpdateYhdMerchandiseOrder")
    public YhdMerchandiseOrder setUpdateYhdMerchandiseOrder(YhdMerchandiseOrder yhdMerchandiseOrder) {
        return this.yhdMerchandiseOrderService.setYhdMerchandiseOrderService(yhdMerchandiseOrder);
    }

    @RequestMapping(value = "/delDeleteYhdMerchandiseOrder/{orderId}", method = RequestMethod.GET)
    public boolean delDeleteYhdMerchandiseOrder(@PathVariable("orderId") int orderId) {
        return this.yhdMerchandiseOrderService.delYhdMerchandiseOrderService(orderId);
    }

}
