package com.item.yhd.controller;

import com.item.yhd.po.YhdMerchandiseSmallIntroduce;
import com.item.yhd.service.YhdMerchandiseSmallIntroduceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/yhdMerchandiseSmallIntroduceController")
public class YhdMerchandiseSmallIntroduceController {

    @Autowired
    private YhdMerchandiseSmallIntroduceService yhdMerchandiseSmallIntroduceService;


    @RequestMapping(value = "/getShowListSmallDetailedIdYhdMerchandiseSmallIntroduce/{smallDetailedId}", method = RequestMethod.GET)
    public List<YhdMerchandiseSmallIntroduce> getShowListSmallDetailedIdYhdMerchandiseSmallIntroduce(@PathVariable("smallDetailedId") int smallDetailedId) {
        return this.yhdMerchandiseSmallIntroduceService.getListSmallDetailedIdYhdMerchandiseSmallIntroduceService(smallDetailedId);
    }

    @RequestMapping(value = "/addInsertYhdMerchandiseSmallIntroduce")
    public YhdMerchandiseSmallIntroduce addInsertYhdMerchandiseSmallIntroduce(YhdMerchandiseSmallIntroduce yhdMerchandiseSmallIntroduce) {
        return this.yhdMerchandiseSmallIntroduceService.addYhdMerchandiseSmallIntroduceService(yhdMerchandiseSmallIntroduce);
    }

    @RequestMapping(value = "/setUpdateYhdMerchandiseSmallIntroduce")
    public YhdMerchandiseSmallIntroduce setUpdateYhdMerchandiseSmallIntroduce(YhdMerchandiseSmallIntroduce yhdMerchandiseSmallIntroduce) {
        return this.yhdMerchandiseSmallIntroduceService.setYhdMerchandiseSmallIntroduceService(yhdMerchandiseSmallIntroduce);
    }

    @RequestMapping(value = "/delDeleteYhdMerchandiseSmallIntroduce/{introduceId}", method = RequestMethod.GET)
    public boolean delDeleteYhdMerchandiseSmallIntroduce(@PathVariable("introduceId") int introduceId) {
        return this.yhdMerchandiseSmallIntroduceService.delYhdMerchandiseSmallIntroduceService(introduceId);
    }

}
