package com.item.yhd.controller;

import com.item.yhd.po.YhdMerchandiseLargeDetailed;
import com.item.yhd.service.YhdMerchandiseLargeDetailedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/yhdMerchandiseLargeDetailedController")
public class YhdMerchandiseLargeDetailedController {

    @Autowired
    private YhdMerchandiseLargeDetailedService yhdMerchandiseLargeDetailedService;


    @RequestMapping(value = "/getPageYhdMerchandiseLargeDetailed/{page}/{size}", method = RequestMethod.GET)
    public Map<String, Object> getPageYhdMerchandiseLargeDetailed(@PathVariable("page") int page, @PathVariable("size") int size) {
        return this.yhdMerchandiseLargeDetailedService.getPageYhdMerchandiseLargeDetailedService(page, size);
    }

    @RequestMapping(value = "/getOneYhdMerchandiseLargeDetailed/{largeDetailedId}", method = RequestMethod.GET)
    public YhdMerchandiseLargeDetailed getOneYhdMerchandiseLargeDetailed(@PathVariable("largeDetailedId") int largeDetailedId) {
        return this.yhdMerchandiseLargeDetailedService.getOneYhdMerchandiseLargeDetailedService(largeDetailedId);
    }

    @RequestMapping(value = "/addYhdMerchandiseLargeDetailed", method = RequestMethod.GET)
    public YhdMerchandiseLargeDetailed addYhdMerchandiseLargeDetailed(YhdMerchandiseLargeDetailed yhdMerchandiseLargeDetailed) {
        return this.yhdMerchandiseLargeDetailedService.addYhdMerchandiseLargeDetailedService(yhdMerchandiseLargeDetailed);
    }

    @RequestMapping(value = "/setYhdMerchandiseLargeDetailed", method = RequestMethod.GET)
    public YhdMerchandiseLargeDetailed setYhdMerchandiseLargeDetailed(YhdMerchandiseLargeDetailed yhdMerchandiseLargeDetailed) {
        return this.yhdMerchandiseLargeDetailedService.setYhdMerchandiseLargeDetailedService(yhdMerchandiseLargeDetailed);
    }

    @RequestMapping(value = "/delYhdMerchandiseLargeDetailed/{largeDetailedId}", method = RequestMethod.GET)
    public boolean delYhdMerchandiseLargeDetailed(@PathVariable("largeDetailedId") int largeDetailedId) {
        return this.yhdMerchandiseLargeDetailedService.delYhdMerchandiseLargeDetailedService(largeDetailedId);
    }

}
