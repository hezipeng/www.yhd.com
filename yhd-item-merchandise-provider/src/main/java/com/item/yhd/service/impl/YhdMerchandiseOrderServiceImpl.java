package com.item.yhd.service.impl;

import com.item.yhd.dao.YhdMerchandiseOrderDao;
import com.item.yhd.po.YhdMerchandiseOrder;
import com.item.yhd.service.YhdMerchandiseOrderService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.Map;

@Service("yhdMerchandiseOrderServiceImpl")
public class YhdMerchandiseOrderServiceImpl implements YhdMerchandiseOrderService {

    @Resource(name = "yhdMerchandiseOrderDao")
    private YhdMerchandiseOrderDao yhdMerchandiseOrderDao;


    @Override
    public Map<String, Object> getPageYhdMerchandiseOrderService(int page, int size, int userId) {
        Map<String, Object> map = new HashMap<String, Object>();
        Specification<YhdMerchandiseOrder> spec = new Specification<YhdMerchandiseOrder>() {
            @Override
            public Predicate toPredicate(Root<YhdMerchandiseOrder> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("userId"), userId);
            }
        };

        Pageable pageable = new PageRequest(page, size);
        Page<YhdMerchandiseOrder> pages = this.yhdMerchandiseOrderDao.findAll(spec, pageable);
        map.put("getTotal", pages.getTotalElements());
        map.put("getPages", pages.getTotalPages());
        map.put("list", pages.getContent());

        return map;
    }

    @Override
    public YhdMerchandiseOrder getOneYhdMerchandiseOrderService(String orderId) {
        return this.yhdMerchandiseOrderDao.findByOrderId(orderId);
    }

    @Override
    public YhdMerchandiseOrder addYhdMerchandiseOrderService(YhdMerchandiseOrder yhdMerchandiseOrder) {
        return this.yhdMerchandiseOrderDao.save(yhdMerchandiseOrder);
    }

    @Override
    public YhdMerchandiseOrder setYhdMerchandiseOrderService(YhdMerchandiseOrder yhdMerchandiseOrder) {
        return this.yhdMerchandiseOrderDao.saveAndFlush(yhdMerchandiseOrder);
    }

    @Override
    public boolean delYhdMerchandiseOrderService(int orderId) {
        try {
            this.yhdMerchandiseOrderDao.delete(orderId);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
