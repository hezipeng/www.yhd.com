package com.item.yhd.service.impl;

import com.item.yhd.dao.YhdMerchandiseOrderDetailedDao;
import com.item.yhd.po.YhdMerchandiseOrderDetailed;
import com.item.yhd.service.YhdMerchandiseOrderDetailedService;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Service("yhdMerchandiseOrderDetailedServiceImpl")
public class YhdMerchandiseOrderDetailedServiceImpl implements YhdMerchandiseOrderDetailedService {

    @Resource(name = "yhdMerchandiseOrderDetailedDao")
    private YhdMerchandiseOrderDetailedDao yhdMerchandiseOrderDetailedDao;


    @Override
    public List<YhdMerchandiseOrderDetailed> getListYhdMerchandiseOrderDetailedService(String orderId) {
        Specification<YhdMerchandiseOrderDetailed> spec = new Specification<YhdMerchandiseOrderDetailed>() {
            @Override
            public Predicate toPredicate(Root<YhdMerchandiseOrderDetailed> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("orderId"), orderId);
            }
        };
        return this.yhdMerchandiseOrderDetailedDao.findAll(spec);
    }

    @Override
    public YhdMerchandiseOrderDetailed addYhdMerchandiseOrderDetailedService(YhdMerchandiseOrderDetailed yhdMerchandiseOrderDetailed) {
        return this.yhdMerchandiseOrderDetailedDao.save(yhdMerchandiseOrderDetailed);
    }

    @Override
    public YhdMerchandiseOrderDetailed setYhdMerchandiseOrderDetailedService(YhdMerchandiseOrderDetailed yhdMerchandiseOrderDetailed) {
        return this.yhdMerchandiseOrderDetailedDao.saveAndFlush(yhdMerchandiseOrderDetailed);
    }

    @Override
    public boolean delYhdMerchandiseOrderDetailedService(String orderId, int largeDetailedId, int smallDetailedId) {
        try {
            this.yhdMerchandiseOrderDetailedDao.deleteByOrderIdAndLargeDetailedIdAndSmallDetailedId(orderId, largeDetailedId, smallDetailedId);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
