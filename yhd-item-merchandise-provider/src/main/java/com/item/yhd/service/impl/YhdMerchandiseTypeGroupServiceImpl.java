package com.item.yhd.service.impl;

import com.item.yhd.dao.YhdMerchandiseTypeGroupDao;
import com.item.yhd.po.YhdMerchandiseTypeGroup;
import com.item.yhd.service.YhdMerchandiseTypeGroupService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("yhdMerchandiseTypeGroupServiceImpl")
public class YhdMerchandiseTypeGroupServiceImpl implements YhdMerchandiseTypeGroupService {

    @Resource(name = "yhdMerchandiseTypeGroupDao")
    private YhdMerchandiseTypeGroupDao yhdMerchandiseTypeGroupDao;


    @Override
    public List<YhdMerchandiseTypeGroup> getListYhdMerchandiseTypeGroupService() {
        return this.yhdMerchandiseTypeGroupDao.findAll();
    }

    @Override
    public YhdMerchandiseTypeGroup addYhdMerchandiseTypeGroupService(YhdMerchandiseTypeGroup yhdMerchandiseTypeGroup) {
        return this.yhdMerchandiseTypeGroupDao.save(yhdMerchandiseTypeGroup);
    }

    @Override
    public YhdMerchandiseTypeGroup setYhdMerchandiseTypeGroupService(YhdMerchandiseTypeGroup yhdMerchandiseTypeGroup) {
        return this.yhdMerchandiseTypeGroupDao.saveAndFlush(yhdMerchandiseTypeGroup);
    }

    @Override
    public boolean delYhdMerchandiseTypeGroupService(int groupId) {
        try {
            this.yhdMerchandiseTypeGroupDao.delete(groupId);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
