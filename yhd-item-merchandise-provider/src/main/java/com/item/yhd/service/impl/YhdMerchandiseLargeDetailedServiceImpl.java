package com.item.yhd.service.impl;

import com.item.yhd.dao.YhdMerchandiseLargeDetailedDao;
import com.item.yhd.po.YhdMerchandiseLargeDetailed;
import com.item.yhd.service.YhdMerchandiseLargeDetailedService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("yhdMerchandiseLargeDetailedServiceImpl")
public class YhdMerchandiseLargeDetailedServiceImpl implements YhdMerchandiseLargeDetailedService {

    @Resource(name = "yhdMerchandiseLargeDetailedDao")
    private YhdMerchandiseLargeDetailedDao yhdMerchandiseLargeDetailedDao;


    @Override
    public Map<String, Object> getPageYhdMerchandiseLargeDetailedService(int page, int size) {
        Map<String, Object> map = new HashMap<String, Object>();

        Specification<YhdMerchandiseLargeDetailed> spec = new Specification<YhdMerchandiseLargeDetailed>() {
            @Override
            public Predicate toPredicate(Root<YhdMerchandiseLargeDetailed> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return null;
            }
        };

        Pageable pageable = new PageRequest(page, size);
        Page<YhdMerchandiseLargeDetailed> pages = this.yhdMerchandiseLargeDetailedDao.findAll(pageable);
        map.put("getTotal", pages.getTotalElements());
        map.put("getPages", pages.getTotalPages());
        map.put("list", pages.getContent());

        return map;
    }

    @Override
    public YhdMerchandiseLargeDetailed getOneYhdMerchandiseLargeDetailedService(int largeDetailedId) {
        return this.yhdMerchandiseLargeDetailedDao.findOne(largeDetailedId);
    }

    @Override
    public YhdMerchandiseLargeDetailed addYhdMerchandiseLargeDetailedService(YhdMerchandiseLargeDetailed yhdMerchandiseLargeDetailed) {
        return this.yhdMerchandiseLargeDetailedDao.save(yhdMerchandiseLargeDetailed);
    }

    @Override
    public YhdMerchandiseLargeDetailed setYhdMerchandiseLargeDetailedService(YhdMerchandiseLargeDetailed yhdMerchandiseLargeDetailed) {
        return this.yhdMerchandiseLargeDetailedDao.saveAndFlush(yhdMerchandiseLargeDetailed);
    }

    @Override
    public boolean delYhdMerchandiseLargeDetailedService(int largeDetailedId) {
        try {
            this.yhdMerchandiseLargeDetailedDao.delete(largeDetailedId);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
