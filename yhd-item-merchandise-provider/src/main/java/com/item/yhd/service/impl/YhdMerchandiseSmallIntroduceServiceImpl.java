package com.item.yhd.service.impl;

import com.item.yhd.dao.YhdMerchandiseSmallIntroduceDao;
import com.item.yhd.po.YhdMerchandiseSmallIntroduce;
import com.item.yhd.service.YhdMerchandiseSmallIntroduceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("yhdMerchandiseSmallIntroduceServiceImpl")
public class YhdMerchandiseSmallIntroduceServiceImpl implements YhdMerchandiseSmallIntroduceService {

    @Resource(name = "yhdMerchandiseSmallIntroduceDao")
    private YhdMerchandiseSmallIntroduceDao yhdMerchandiseSmallIntroduceDao;


    @Override
    public List<YhdMerchandiseSmallIntroduce> getListSmallDetailedIdYhdMerchandiseSmallIntroduceService(int smallDetailedId) {
        return this.yhdMerchandiseSmallIntroduceDao.findAllBySmallDetailedId(smallDetailedId);
    }

    @Override
    public YhdMerchandiseSmallIntroduce addYhdMerchandiseSmallIntroduceService(YhdMerchandiseSmallIntroduce yhdMerchandiseSmallIntroduce) {
        return this.yhdMerchandiseSmallIntroduceDao.save(yhdMerchandiseSmallIntroduce);
    }

    @Override
    public YhdMerchandiseSmallIntroduce setYhdMerchandiseSmallIntroduceService(YhdMerchandiseSmallIntroduce yhdMerchandiseSmallIntroduce) {
        return this.yhdMerchandiseSmallIntroduceDao.saveAndFlush(yhdMerchandiseSmallIntroduce);
    }

    @Override
    public boolean delYhdMerchandiseSmallIntroduceService(int introduceId) {
        try {
            this.yhdMerchandiseSmallIntroduceDao.delete(introduceId);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
