package com.item.yhd.service;

import com.item.yhd.po.YhdMerchandiseOrder;

import java.util.Map;

public interface YhdMerchandiseOrderService {

    public Map<String, Object> getPageYhdMerchandiseOrderService(int page, int size, int userId);

    public YhdMerchandiseOrder getOneYhdMerchandiseOrderService(String orderId);

    public YhdMerchandiseOrder addYhdMerchandiseOrderService(YhdMerchandiseOrder yhdMerchandiseOrder);

    public YhdMerchandiseOrder setYhdMerchandiseOrderService(YhdMerchandiseOrder yhdMerchandiseOrder);

    public boolean delYhdMerchandiseOrderService(int orderId);

}
