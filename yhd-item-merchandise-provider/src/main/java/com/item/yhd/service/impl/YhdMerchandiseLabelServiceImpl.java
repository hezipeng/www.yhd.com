package com.item.yhd.service.impl;

import com.item.yhd.dao.YhdMerchandiseLabelDao;
import com.item.yhd.po.YhdMerchandiseLabel;
import com.item.yhd.service.YhdMerchandiseLabelService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("yhdMerchandiseLabelServiceImpl")
public class YhdMerchandiseLabelServiceImpl implements YhdMerchandiseLabelService {

    @Resource(name = "yhdMerchandiseLabelDao")
    private YhdMerchandiseLabelDao yhdMerchandiseLabelDao;


    @Override
    public YhdMerchandiseLabel getOneYhdMerchandiseLabelService(int merchandiseLabelId) {
        return this.yhdMerchandiseLabelDao.findOne(merchandiseLabelId);
    }

    @Override
    public YhdMerchandiseLabel addYhdMerchandiseLabelService(YhdMerchandiseLabel yhdMerchandiseLabel) {
        return this.yhdMerchandiseLabelDao.save(yhdMerchandiseLabel);
    }

    @Override
    public YhdMerchandiseLabel setYhdMerchandiseLabelService(YhdMerchandiseLabel yhdMerchandiseLabel) {
        return this.yhdMerchandiseLabelDao.saveAndFlush(yhdMerchandiseLabel);
    }

    @Override
    public boolean delYhdMerchandiseLabelService(int merchandiseLabelId) {
        try {
            this.yhdMerchandiseLabelDao.delete(merchandiseLabelId);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
