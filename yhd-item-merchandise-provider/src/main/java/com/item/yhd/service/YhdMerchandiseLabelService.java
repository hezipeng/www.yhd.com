package com.item.yhd.service;

import com.item.yhd.po.YhdMerchandiseLabel;

public interface YhdMerchandiseLabelService {

    /**
     * 获得产品标签
     * @param merchandiseLabelId
     * @return
     */
    public YhdMerchandiseLabel getOneYhdMerchandiseLabelService(int merchandiseLabelId);

    /**
     * 添加产品标签
     * @param yhdMerchandiseLabel
     * @return
     */
    public YhdMerchandiseLabel addYhdMerchandiseLabelService(YhdMerchandiseLabel yhdMerchandiseLabel);

    /**
     * 修改产品标签
     * @param yhdMerchandiseLabel
     * @return
     */
    public YhdMerchandiseLabel setYhdMerchandiseLabelService(YhdMerchandiseLabel yhdMerchandiseLabel);

    /**
     * 删除产品标签
     * @param merchandiseLabelId
     * @return
     */
    public boolean delYhdMerchandiseLabelService(int merchandiseLabelId);

}
