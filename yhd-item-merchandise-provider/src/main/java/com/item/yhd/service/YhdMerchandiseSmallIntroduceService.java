package com.item.yhd.service;

import com.item.yhd.po.YhdMerchandiseSmallIntroduce;

import java.util.List;

public interface YhdMerchandiseSmallIntroduceService {

    public List<YhdMerchandiseSmallIntroduce> getListSmallDetailedIdYhdMerchandiseSmallIntroduceService(int smallDetailedId);

    public YhdMerchandiseSmallIntroduce addYhdMerchandiseSmallIntroduceService(YhdMerchandiseSmallIntroduce yhdMerchandiseSmallIntroduce);

    public YhdMerchandiseSmallIntroduce setYhdMerchandiseSmallIntroduceService(YhdMerchandiseSmallIntroduce yhdMerchandiseSmallIntroduce);

    public boolean delYhdMerchandiseSmallIntroduceService(int introduceId);

}
