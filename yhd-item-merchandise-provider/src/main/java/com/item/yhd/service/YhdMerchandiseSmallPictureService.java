package com.item.yhd.service;

import com.item.yhd.po.YhdMerchandiseSmallPicture;

import java.util.List;

public interface YhdMerchandiseSmallPictureService {

    public List<YhdMerchandiseSmallPicture> getListSmallDetailedIdYhdMerchandiseSmallPictureService(int smallDetailedId);

    public YhdMerchandiseSmallPicture addYhdMerchandiseSmallPictureService(YhdMerchandiseSmallPicture yhdMerchandiseSmallPicture);

    public YhdMerchandiseSmallPicture setYhdMerchandiseSmallPictureService(YhdMerchandiseSmallPicture yhdMerchandiseSmallPicture);

    public boolean delYhdMerchandiseSmallPictureService(int pictureId);

}
