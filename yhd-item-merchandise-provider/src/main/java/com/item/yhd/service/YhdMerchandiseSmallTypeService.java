package com.item.yhd.service;

import com.item.yhd.po.YhdMerchandiseSmallType;

import java.util.List;

public interface YhdMerchandiseSmallTypeService {

    public List<YhdMerchandiseSmallType> getListLargeTypeIdYhdMerchandiseSmallTypeService(int largeTypeId);

    public YhdMerchandiseSmallType addYhdMerchandiseSmallTypeService(YhdMerchandiseSmallType yhdMerchandiseSmallType);

    public YhdMerchandiseSmallType setYhdMerchandiseSmallTypeService(YhdMerchandiseSmallType yhdMerchandiseSmallType);

    public boolean delYhdMerchandiseSmallTypeService(int smallTypeId);

}
