package com.item.yhd.service.impl;

import com.item.yhd.dao.YhdMerchandiseSmallTypeDao;
import com.item.yhd.po.YhdMerchandiseSmallType;
import com.item.yhd.service.YhdMerchandiseSmallTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("yhdMerchandiseSmallTypeServiceImpl")
public class YhdMerchandiseSmallTypeServiceImpl implements YhdMerchandiseSmallTypeService {

    @Resource(name = "yhdMerchandiseSmallTypeDao")
    private YhdMerchandiseSmallTypeDao yhdMerchandiseSmallTypeDao;


    @Override
    public List<YhdMerchandiseSmallType> getListLargeTypeIdYhdMerchandiseSmallTypeService(int largeTypeId) {
        return this.yhdMerchandiseSmallTypeDao.findAllByLargeTypeId(largeTypeId);
    }

    @Override
    public YhdMerchandiseSmallType addYhdMerchandiseSmallTypeService(YhdMerchandiseSmallType yhdMerchandiseSmallType) {
        return this.yhdMerchandiseSmallTypeDao.save(yhdMerchandiseSmallType);
    }

    @Override
    public YhdMerchandiseSmallType setYhdMerchandiseSmallTypeService(YhdMerchandiseSmallType yhdMerchandiseSmallType) {
        return this.yhdMerchandiseSmallTypeDao.saveAndFlush(yhdMerchandiseSmallType);
    }

    @Override
    public boolean delYhdMerchandiseSmallTypeService(int smallTypeId) {
        try {
            this.yhdMerchandiseSmallTypeDao.delete(smallTypeId);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
