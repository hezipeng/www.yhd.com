package com.item.yhd.service;

import com.item.yhd.po.YhdMerchandiseLargeDetailed;

import java.util.List;
import java.util.Map;

public interface YhdMerchandiseLargeDetailedService {

    public Map<String, Object> getPageYhdMerchandiseLargeDetailedService(int page, int size);

    public YhdMerchandiseLargeDetailed getOneYhdMerchandiseLargeDetailedService(int largeDetailedId);

    public YhdMerchandiseLargeDetailed addYhdMerchandiseLargeDetailedService(YhdMerchandiseLargeDetailed yhdMerchandiseLargeDetailed);

    public YhdMerchandiseLargeDetailed setYhdMerchandiseLargeDetailedService(YhdMerchandiseLargeDetailed yhdMerchandiseLargeDetailed);

    public boolean delYhdMerchandiseLargeDetailedService(int largeDetailedId);

}
