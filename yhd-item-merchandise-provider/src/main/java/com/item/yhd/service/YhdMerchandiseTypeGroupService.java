package com.item.yhd.service;

import com.item.yhd.po.YhdMerchandiseTypeGroup;

import java.util.List;

public interface YhdMerchandiseTypeGroupService {

    public List<YhdMerchandiseTypeGroup> getListYhdMerchandiseTypeGroupService();

    public YhdMerchandiseTypeGroup addYhdMerchandiseTypeGroupService(YhdMerchandiseTypeGroup yhdMerchandiseTypeGroup);

    public YhdMerchandiseTypeGroup setYhdMerchandiseTypeGroupService(YhdMerchandiseTypeGroup yhdMerchandiseTypeGroup);

    public boolean delYhdMerchandiseTypeGroupService(int groupId);

}
