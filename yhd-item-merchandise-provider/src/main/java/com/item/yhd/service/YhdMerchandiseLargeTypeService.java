package com.item.yhd.service;

import com.item.yhd.po.YhdMerchandiseLargeType;

import java.util.List;

public interface YhdMerchandiseLargeTypeService {

    public List<YhdMerchandiseLargeType> getListYhdMerchandiseLargeTypeService();

    public YhdMerchandiseLargeType getOneYhdMerchandiseLargeTypeService(int largeTypeId);

    public YhdMerchandiseLargeType addYhdMerchandiseLargeTypeService(YhdMerchandiseLargeType yhdMerchandiseLargeType);

    public YhdMerchandiseLargeType setYhdMerchandiseLargeTypeService(YhdMerchandiseLargeType yhdMerchandiseLargeType);

    public boolean delYhdMerchandiseLargeTypeService(int largeTypeId);

}
