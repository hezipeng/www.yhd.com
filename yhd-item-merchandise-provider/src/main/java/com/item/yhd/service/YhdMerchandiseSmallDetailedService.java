package com.item.yhd.service;

import com.item.yhd.po.YhdMerchandiseSmallDetailed;

import java.util.List;

public interface YhdMerchandiseSmallDetailedService {

    public List<YhdMerchandiseSmallDetailed> getListLargeDetailedIdYhdMerchandiseSmallDetailedService(int largeDetailedId);

    public YhdMerchandiseSmallDetailed getOneYhdMerchandiseSmallDetailedService(int smallDetailedId);

    public YhdMerchandiseSmallDetailed addYhdMerchandiseSmallDetailedService(YhdMerchandiseSmallDetailed yhdMerchandiseSmallDetailed);

    public YhdMerchandiseSmallDetailed setYhdMerchandiseSmallDetailedService(YhdMerchandiseSmallDetailed yhdMerchandiseSmallDetailed);

    public boolean delYhdMerchandiseSmallDetailedService(int smallDetailedId);

}
