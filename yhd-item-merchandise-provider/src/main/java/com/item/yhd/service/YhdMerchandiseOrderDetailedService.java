package com.item.yhd.service;

import com.item.yhd.po.YhdMerchandiseOrderDetailed;

import java.util.List;

public interface YhdMerchandiseOrderDetailedService {

    public List<YhdMerchandiseOrderDetailed> getListYhdMerchandiseOrderDetailedService(String orderId);

    public YhdMerchandiseOrderDetailed addYhdMerchandiseOrderDetailedService(YhdMerchandiseOrderDetailed yhdMerchandiseOrderDetailed);

    public YhdMerchandiseOrderDetailed setYhdMerchandiseOrderDetailedService(YhdMerchandiseOrderDetailed yhdMerchandiseOrderDetailed);

    public boolean delYhdMerchandiseOrderDetailedService(String orderId, int largeDetailedId, int smallDetailedId);

}
