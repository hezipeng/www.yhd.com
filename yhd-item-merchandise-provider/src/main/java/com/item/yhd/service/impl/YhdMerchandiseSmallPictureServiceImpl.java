package com.item.yhd.service.impl;

import com.item.yhd.dao.YhdMerchandiseSmallPictureDao;
import com.item.yhd.po.YhdMerchandiseSmallPicture;
import com.item.yhd.service.YhdMerchandiseSmallPictureService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("yhdMerchandiseSmallPictureServiceImpl")
public class YhdMerchandiseSmallPictureServiceImpl implements YhdMerchandiseSmallPictureService {

    @Resource(name = "yhdMerchandiseSmallPictureDao")
    private YhdMerchandiseSmallPictureDao yhdMerchandiseSmallPictureDao;


    @Override
    public List<YhdMerchandiseSmallPicture> getListSmallDetailedIdYhdMerchandiseSmallPictureService(int smallDetailedId) {
        return this.yhdMerchandiseSmallPictureDao.findAllBySmallDetailedId(smallDetailedId);
    }

    @Override
    public YhdMerchandiseSmallPicture addYhdMerchandiseSmallPictureService(YhdMerchandiseSmallPicture yhdMerchandiseSmallPicture) {
        return this.yhdMerchandiseSmallPictureDao.save(yhdMerchandiseSmallPicture);
    }

    @Override
    public YhdMerchandiseSmallPicture setYhdMerchandiseSmallPictureService(YhdMerchandiseSmallPicture yhdMerchandiseSmallPicture) {
        return this.yhdMerchandiseSmallPictureDao.saveAndFlush(yhdMerchandiseSmallPicture);
    }

    @Override
    public boolean delYhdMerchandiseSmallPictureService(int pictureId) {
        try {
            this.yhdMerchandiseSmallPictureDao.delete(pictureId);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
