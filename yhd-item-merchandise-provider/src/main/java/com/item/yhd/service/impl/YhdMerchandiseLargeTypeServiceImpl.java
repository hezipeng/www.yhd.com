package com.item.yhd.service.impl;

import com.item.yhd.dao.YhdMerchandiseLargeTypeDao;
import com.item.yhd.po.YhdMerchandiseLargeType;
import com.item.yhd.service.YhdMerchandiseLargeTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("yhdMerchandiseLargeTypeServiceImpl")
public class YhdMerchandiseLargeTypeServiceImpl implements YhdMerchandiseLargeTypeService {

    @Resource(name = "yhdMerchandiseLargeTypeDao")
    private YhdMerchandiseLargeTypeDao yhdMerchandiseLargeTypeDao;


    @Override
    public List<YhdMerchandiseLargeType> getListYhdMerchandiseLargeTypeService() {
        return this.yhdMerchandiseLargeTypeDao.findAll();
    }

    @Override
    public YhdMerchandiseLargeType getOneYhdMerchandiseLargeTypeService(int largeTypeId) {
        return this.yhdMerchandiseLargeTypeDao.findOne(largeTypeId);
    }

    @Override
    public YhdMerchandiseLargeType addYhdMerchandiseLargeTypeService(YhdMerchandiseLargeType yhdMerchandiseLargeType) {
        return this.yhdMerchandiseLargeTypeDao.save(yhdMerchandiseLargeType);
    }

    @Override
    public YhdMerchandiseLargeType setYhdMerchandiseLargeTypeService(YhdMerchandiseLargeType yhdMerchandiseLargeType) {
        return this.yhdMerchandiseLargeTypeDao.saveAndFlush(yhdMerchandiseLargeType);
    }

    @Override
    public boolean delYhdMerchandiseLargeTypeService(int largeTypeId) {
        try {
            this.yhdMerchandiseLargeTypeDao.delete(largeTypeId);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
