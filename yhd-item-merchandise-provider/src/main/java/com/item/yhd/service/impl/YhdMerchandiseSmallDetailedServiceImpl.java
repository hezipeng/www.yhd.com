package com.item.yhd.service.impl;

import com.item.yhd.dao.YhdMerchandiseSmallDetailedDao;
import com.item.yhd.po.YhdMerchandiseSmallDetailed;
import com.item.yhd.service.YhdMerchandiseSmallDetailedService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("yhdMerchandiseSmallDetailedServiceImpl")
public class YhdMerchandiseSmallDetailedServiceImpl implements YhdMerchandiseSmallDetailedService {

    @Resource(name = "yhdMerchandiseSmallDetailedDao")
    private YhdMerchandiseSmallDetailedDao yhdMerchandiseSmallDetailedDao;


    @Override
    public List<YhdMerchandiseSmallDetailed> getListLargeDetailedIdYhdMerchandiseSmallDetailedService(int largeDetailedId) {
        return this.yhdMerchandiseSmallDetailedDao.findAllByLargeDetailedId(largeDetailedId);
    }

    @Override
    public YhdMerchandiseSmallDetailed getOneYhdMerchandiseSmallDetailedService(int smallDetailedId) {
        return this.yhdMerchandiseSmallDetailedDao.findOne(smallDetailedId);
    }

    @Override
    public YhdMerchandiseSmallDetailed addYhdMerchandiseSmallDetailedService(YhdMerchandiseSmallDetailed yhdMerchandiseSmallDetailed) {
        return this.yhdMerchandiseSmallDetailedDao.save(yhdMerchandiseSmallDetailed);
    }

    @Override
    public YhdMerchandiseSmallDetailed setYhdMerchandiseSmallDetailedService(YhdMerchandiseSmallDetailed yhdMerchandiseSmallDetailed) {
        return this.yhdMerchandiseSmallDetailedDao.saveAndFlush(yhdMerchandiseSmallDetailed);
    }

    @Override
    public boolean delYhdMerchandiseSmallDetailedService(int smallDetailedId) {
        try {
            this.yhdMerchandiseSmallDetailedDao.delete(smallDetailedId);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
