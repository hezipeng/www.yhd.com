package com.item.yhd.dao;

import com.item.yhd.po.YhdMerchandiseSmallPicture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("yhdMerchandiseSmallPictureDao")
public interface YhdMerchandiseSmallPictureDao extends JpaRepository<YhdMerchandiseSmallPicture, Integer>, JpaSpecificationExecutor<YhdMerchandiseSmallPicture> {

    public List<YhdMerchandiseSmallPicture> findAllBySmallDetailedId(int smallDetailedId);

}
