package com.item.yhd.dao;

import com.item.yhd.po.YhdMerchandiseLargeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository("yhdMerchandiseLargeTypeDao")
public interface YhdMerchandiseLargeTypeDao extends JpaRepository<YhdMerchandiseLargeType, Integer>, JpaSpecificationExecutor<YhdMerchandiseLargeType> {

}
