package com.item.yhd.dao;

import com.item.yhd.po.YhdMerchandiseSmallType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("yhdMerchandiseSmallTypeDao")
public interface YhdMerchandiseSmallTypeDao extends JpaRepository<YhdMerchandiseSmallType, Integer>, JpaSpecificationExecutor<YhdMerchandiseSmallType> {

    public List<YhdMerchandiseSmallType> findAllByLargeTypeId(int largeTypeId);

}
