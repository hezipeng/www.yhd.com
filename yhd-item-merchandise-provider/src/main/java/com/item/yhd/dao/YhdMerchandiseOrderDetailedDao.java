package com.item.yhd.dao;

import com.item.yhd.po.YhdMerchandiseOrderDetailed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository("yhdMerchandiseOrderDetailedDao")
public interface YhdMerchandiseOrderDetailedDao extends JpaRepository<YhdMerchandiseOrderDetailed, Integer>, JpaSpecificationExecutor<YhdMerchandiseOrderDetailed> {

    public void deleteByOrderIdAndLargeDetailedIdAndSmallDetailedId(String orderId, int largeDetailedId, int smallDetailedId);

}
