package com.item.yhd.dao;

import com.item.yhd.po.YhdMerchandiseLargeDetailed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository("yhdMerchandiseLargeDetailedDao")
public interface YhdMerchandiseLargeDetailedDao extends JpaRepository<YhdMerchandiseLargeDetailed, Integer>, JpaSpecificationExecutor<YhdMerchandiseLargeDetailed> {

}
