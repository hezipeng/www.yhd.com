package com.item.yhd.dao;

import com.item.yhd.po.YhdMerchandiseTypeGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository("yhdMerchandiseTypeGroupDao")
public interface YhdMerchandiseTypeGroupDao extends JpaRepository<YhdMerchandiseTypeGroup, Integer>, JpaSpecificationExecutor<YhdMerchandiseTypeGroup> {

}
