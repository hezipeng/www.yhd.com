package com.item.yhd.dao;

import com.item.yhd.po.YhdMerchandiseSmallIntroduce;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("yhdMerchandiseSmallIntroduceDao")
public interface YhdMerchandiseSmallIntroduceDao extends JpaRepository<YhdMerchandiseSmallIntroduce, Integer>, JpaSpecificationExecutor<YhdMerchandiseSmallIntroduce> {

    public List<YhdMerchandiseSmallIntroduce> findAllBySmallDetailedId(int smallDetailedId);

}
