package com.item.yhd.dao;

import com.item.yhd.po.YhdMerchandiseOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository("yhdMerchandiseOrderDao")
public interface YhdMerchandiseOrderDao extends JpaRepository<YhdMerchandiseOrder, Integer>, JpaSpecificationExecutor<YhdMerchandiseOrder> {

    public YhdMerchandiseOrder findByOrderId(String orderId);

}
