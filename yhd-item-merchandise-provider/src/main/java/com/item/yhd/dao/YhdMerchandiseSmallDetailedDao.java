package com.item.yhd.dao;

import com.item.yhd.po.YhdMerchandiseSmallDetailed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("yhdMerchandiseSmallDetailedDao")
public interface YhdMerchandiseSmallDetailedDao extends JpaRepository<YhdMerchandiseSmallDetailed, Integer>, JpaSpecificationExecutor<YhdMerchandiseSmallDetailed> {

    public List<YhdMerchandiseSmallDetailed> findAllByLargeDetailedId(int largeDetailedId);

}
