package com.item.yhd.dao;

import com.item.yhd.po.YhdMerchandiseLabel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository("yhdMerchandiseLabelDao")
public interface YhdMerchandiseLabelDao extends JpaRepository<YhdMerchandiseLabel, Integer>, JpaSpecificationExecutor<YhdMerchandiseLabel> {

}
