package com.item.yhd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YhdItemMerchandiseProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(YhdItemMerchandiseProviderApplication.class, args);
    }

}
