package com.item.yhd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YhdItemConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(YhdItemConfigApplication.class, args);
    }

}
