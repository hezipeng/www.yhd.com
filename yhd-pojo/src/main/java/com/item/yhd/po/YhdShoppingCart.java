package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 购物车表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_shopping_cart")
public class YhdShoppingCart implements java.io.Serializable {

  @Id
  private int id;

  @Column(name = "user_id")
  private int userId;

  @Column(name = "large_detailed_id")
  private int largeDetailedId;

  @Column(name = "small_detailed_id")
  private int smallDetailedId;

  @Column(name = "shopping_cart_price")
  private double shoppingCartPrice;

  @Column(name = "shopping_cart_number")
  private int shoppingCartNumber;

}
