package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;

/**
 * 产品小类型表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_merchandise_small_type")
@Proxy(lazy = false)
public class YhdMerchandiseSmallType implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "small_type_id")
  private int smallTypeId;

  @Column(name = "large_type_id")
  private int largeTypeId;

  @Column(name = "small_type_name")
  private String smallTypeName;

}
