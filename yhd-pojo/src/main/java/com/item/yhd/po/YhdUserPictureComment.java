package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;

/**
 * 用户评价图片表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_user_picture_comment")
@Proxy(lazy = false)
public class YhdUserPictureComment implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "picture_comment_id")
  private int pictureCommentId;

  @Column(name = "comment_id")
  private int commentId;

  @Column(name = "comment_picture")
  private String commentPicture;

}
