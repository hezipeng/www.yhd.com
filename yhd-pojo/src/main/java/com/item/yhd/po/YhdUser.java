package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;

/**
 * 用户表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_user")
@Proxy(lazy = false)
public class YhdUser implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "user_id")
  private int userId;

  @Column(name = "user_account")
  private String userAccount;

  @Column(name = "user_password")
  private String userPassword;

  @Column(name = "user_datetime")
  private String userDatetime;

  @Column(name = "user_phone")
  private String userPhone;

  @Column(name = "user_email")
  private String userEmail;

}
