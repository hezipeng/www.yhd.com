package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;

/**
 * 产品标签表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_merchandise_label")
@Proxy(lazy = false)
public class YhdMerchandiseLabel implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "merchandise_label_id")
  private int merchandiseLabelId;

  @Column(name = "merchandise_label_name")
  private String merchandiseLabelName;

  @Column(name = "merchandise_label_datetime")
  private String merchandiseLabelDatetime;

  @Column(name = "merchandise_label_picture")
  private String merchandiseLabelPicture;

  @Column(name = "merchandise_label_whether_show")
  private int merchandiseLabelWhetherShow;

}
