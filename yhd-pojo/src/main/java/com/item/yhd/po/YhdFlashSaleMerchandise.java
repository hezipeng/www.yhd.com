package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 限时抢购产品详细表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_flash_sale_merchandise")
public class YhdFlashSaleMerchandise implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "sale_merchandise_id")
  private int saleMerchandiseId;

  @Column(name = "sale_id")
  private int saleId;

  @ManyToOne
  @JoinColumn(name = "shop_id")
  private YhdShop yhdShop;

  @Column(name = "large_detailed_id")
  private int largeDetailedId;

  @Column(name = "small_detailed_id")
  private int smallDetailedId;

}
