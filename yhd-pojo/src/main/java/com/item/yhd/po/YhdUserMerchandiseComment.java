package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;

/**
 * 用户对产品评价表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_user_merchandise_comment")
@Proxy(lazy = false)
public class YhdUserMerchandiseComment implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "comment_id")
  private int commentId;

  @Column(name = "user_id")
  private int userId;

  @Column(name = "large_detailed_id")
  private int largeDetailedId;

  @Column(name = "small_detailed_id")
  private int smallDetailedId;

  @Column(name = "comment_score")
  private double commentScore;

  @Column(name = "comment_content")
  private String commentContent;

  @Column(name = "comment_reply")
  private String commentReply;

  @Column(name = "comment_score_date")
  private String commentScoreDate;

  @Column(name = "comment_reply_date")
  private String commentReplyDate;

  @Column(name = "comment_whether_show")
  private int commentWhetherShow;

  @Column(name = "comment_valid_state")
  private int commentValidState;

}
