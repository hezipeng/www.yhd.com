package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 店铺可经营类型表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_shop_operate_type")
public class YhdShopOperateType implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "operate_type_id")
  private int operateTypeId;

  @Column(name = "shop_id")
  private int shopId;

  @Column(name = "large_type_id")
  private int largeTypeId;

}
