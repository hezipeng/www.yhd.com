package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 店铺物流表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_shop_logistics")
public class YhdShopLogistics implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "shop_logistics_id")
  private int shopLogisticsId;

  @Column(name = "shop_id")
  private int shopId;

  @Column(name = "logistics_id")
  private int logisticsId;

  @Column(name = "logistics_whether_show")
  private int logisticsWhetherShow;

}
