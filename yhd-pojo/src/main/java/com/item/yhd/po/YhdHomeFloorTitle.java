package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 首页楼层主标题分类表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_home_floor_title")
public class YhdHomeFloorTitle implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "floor_title_id")
  private int floorTitleId;

  @Column(name = "floor_id")
  private int floorId;

  @Column(name = "small_type_id")
  private int smallTypeId;;

}
