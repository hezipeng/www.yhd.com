package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 首页楼层产品列表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_home_floor_merchandise")
public class YhdHomeFloorMerchandise implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "floor_merchandise_id")
  private int floorMerchandiseId;

  @ManyToOne
  @JoinColumn(name = "floor_id")
  private YhdHomeFloor yhdHomeFloor;

  @Column(name = "large_detailed_id")
  private int largeDetailedId;

  @Column(name = "small_detailed_id")
  private int smallDetailedId;

}
