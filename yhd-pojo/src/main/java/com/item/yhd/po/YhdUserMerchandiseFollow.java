package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;

/**
 * 用户关注的商品表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_user_merchandise_follow")
@Proxy(lazy = false)
public class YhdUserMerchandiseFollow implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "follow_id")
  private int followId;

  @Column(name = "user_id")
  private int userId;

  @Column(name = "large_detailed_id")
  private int largeDetailedId;

  @Column(name = "small_detailed_id")
  private int smallDetailedId;

}
