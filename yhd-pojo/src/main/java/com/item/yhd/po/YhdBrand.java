package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 品牌表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_brand")
public class YhdBrand implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "brand_id")
  private int brandId;

  @Column(name = "large_type_id")
  private int largeTypeId;

  @Column(name = "brand_chinese_name")
  private String brandChineseName;

  @Column(name = "brand_english_name")
  private String brandEnglishName;

  @Column(name = "brand_initial")
  private String brandInitial;

  @Column(name = "brand_logo_picture")
  private String brandLogoPicture;

  @Column(name = "brand_whether_prohibit")
  private int brandWhetherProhibit;

}
