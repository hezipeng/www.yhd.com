package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;

/**
 * 产品类型组
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_merchandise_type_group")
@Proxy(lazy = false)
public class YhdMerchandiseTypeGroup implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "group_id")
  private int groupId;

  @Column(name = "group_name")
  private String groupName;

  @Column(name = "large_type_id")
  private int largeTypeId;

}
