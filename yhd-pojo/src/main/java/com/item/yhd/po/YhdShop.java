package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 店铺表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_shop")
public class YhdShop implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "shop_id")
  private int shopId;

  @Column(name = "user_id")
  private int userId;

  @Column(name = "shop_type_id")
  private int shopTypeId;

  @Column(name = "shop_grade_id")
  private int shopGradeId;

  @Column(name = "shop_name")
  private String shopName;

  @Column(name = "shop_picture")
  private String shopPicture;

  @Column(name = "shop_license")
  private String shopLicense;

  @Column(name = "shop_capital")
  private String shopCapital;

  @Column(name = "shop_tax_registration")
  private String shopTaxRegistration;

  @Column(name = "shop_organization")
  private String shopOrganization;

  @Column(name = "shop_legal_representative_name")
  private String shopLegalRepresentativeName;

  @Column(name = "shop_bank_name")
  private String shopBankName;

  @Column(name = "shop_bank_number")
  private String shopBankNumber;

  @Column(name = "shop_datetime")
  private String shopDatetime;

  @Column(name = "shop_state")
  private int shopState;

}
