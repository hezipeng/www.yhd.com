package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;

/**
 * 小产品介绍表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_merchandise_small_introduce")
@Proxy(lazy = false)
public class YhdMerchandiseSmallIntroduce {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "introduce_id")
  private int introduceId;

  @Column(name = "small_detailed_id")
  private int smallDetailedId;

  @Column(name = "introduce_picture")
  private String introducePicture;

}
