package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 店铺类型表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_shop_type")
public class YhdShopType implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "shop_type_id")
  private int shopTypeId;

  @Column(name = "shop_type_name")
  private String shopTypeName;

  @Column(name = "shop_type_whether_enable")
  private int shopTypeWhetherEnable;

}
