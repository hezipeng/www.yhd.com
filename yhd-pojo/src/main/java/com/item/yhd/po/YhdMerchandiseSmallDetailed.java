package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;

/**
 * 小产品详细表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_merchandise_small_detailed")
@Proxy(lazy = false)
public class YhdMerchandiseSmallDetailed implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "small_detailed_id")
  private int smallDetailedId;

  @Column(name = "large_detailed_id")
  private int largeDetailedId;

  @Column(name = "small_detailed_name")
  private String smallDetailedName;

  @Column(name = "small_detailed_title")
  private String smallDetailedTitle;

  @Column(name = "small_detailed_series")
  private String smallDetailedSeries;

  @Column(name = "small_detailed_spec")
  private String smallDetailedSpec;

  @Column(name = "small_detailed_meal")
  private String smallDetailedMeal;

  @Column(name = "small_detailed_market_price")
  private double smallDetailedMarketPrice;

  @Column(name = "small_detailed_store_price")
  private double smallDetailedStorePrice;

  @Column(name = "small_detailed_number")
  private int smallDetailedNumber;

  @Column(name = "small_detailed_whether_show")
  private int smallDetailedWhetherShow;

  @Column(name = "small_detailed_whether_lower")
  private int smallDetailedWhetherLower;

  @Column(name = "small_detailed_whether_prohibit")
  private int smallDetailedWhetherProhibit;

}
