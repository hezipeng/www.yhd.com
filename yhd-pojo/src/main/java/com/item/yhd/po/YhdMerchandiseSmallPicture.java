package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;

/**
 * 小产品图片表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_merchandise_small_picture")
@Proxy(lazy = false)
public class YhdMerchandiseSmallPicture implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "picture_id")
  private int pictureId;

  @Column(name = "small_detailed_id")
  private int smallDetailedId;

  @Column(name = "picture_address")
  private String pictureAddress;

  @Column(name = "picture_whether_show")
  private int pictureWhetherShow;

}
