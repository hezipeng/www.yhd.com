package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 限时抢购表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_flash_sale")
public class YhdFlashSale implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "sale_id")
  private int saleId;

  @Column(name = "sale_name")
  private String saleName;

  @Column(name = "sale_start")
  private String saleStart;

  @Column(name = "sale_end")
  private String saleEnd;

  @Column(name = "sale_number")
  private int saleNumber;

  @Column(name = "sale_fracture")
  private String saleFracture;

  @Column(name = "sale_state")
  private int saleState;

}
