package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;

/**
 * 大产品详细表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_merchandise_large_detailed")
@Proxy(lazy = false)
public class YhdMerchandiseLargeDetailed implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "large_detailed_id")
  private int largeDetailedId;

  @Column(name = "shop_id")
  private int shopId;

  @Column(name = "small_type_id")
  private int smallTypeId;

  @Column(name = "merchandise_label_id")
  private int merchandiseLabelId;

  @Column(name = "brand_id")
  private int brandId;

  @Column(name = "large_detailed_gross")
  private String largeDetailedGross;

  @Column(name = "large_detailed_place")
  private String largeDetailedPlace;

  @Column(name = "large_detailed_whether_lower")
  private int largeDetailedWhetherLower;

}
