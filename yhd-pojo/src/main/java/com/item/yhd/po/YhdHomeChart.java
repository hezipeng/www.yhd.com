package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 首页轮播图表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_home_chart")
public class YhdHomeChart implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "chart_id")
  private int chartId;

  @Column(name = "chart_name")
  private String chartName;

  @Column(name = "chart_picture")
  private String chartPicture;

  @Column(name = "large_detailed_id")
  private int largeDetailedId;

  @Column(name = "small_detailed_id")
  private int smallDetailedId;

  @Column(name = "chart_order")
  private int chartOrder;

  @Column(name = "chart_whether_show")
  private int chartWhetherShow;

}
