package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;

/**
 * 产品大类型表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_merchandise_large_type")
@Proxy(lazy = false)
public class YhdMerchandiseLargeType implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "large_type_id")
  private int largeTypeId;

  @Column(name = "large_type_name")
  private String largeTypeName;

}
