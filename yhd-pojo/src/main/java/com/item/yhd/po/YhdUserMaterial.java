package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;

/**
 * 用户个人资料表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_user_material")
@Proxy(lazy = false)
public class YhdUserMaterial implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "material_id")
  private int materialId;

  @Column(name = "user_id")
  private int userId;

  @Column(name = "material_portrait")
  private String materialPortrait;

  @Column(name = "material_name")
  private String materialName;

  @Column(name = "material_sex")
  private String materialSex;

  @Column(name = "material_birthday")
  private String materialBirthday;

}
