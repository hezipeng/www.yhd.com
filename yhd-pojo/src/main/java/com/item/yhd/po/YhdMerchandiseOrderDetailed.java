package com.item.yhd.po;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;

/**
 * 产品订单详细表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_merchandise_order_detailed")
@Proxy(lazy = false)
public class YhdMerchandiseOrderDetailed implements java.io.Serializable {

  @Id
  @Column(name = "order_id")
  private String orderId;

  @Column(name = "large_detailed_id")
  private int largeDetailedId;

  @Column(name = "small_detailed_id")
  private int smallDetailedId;

  @Column(name = "order_detailed_one_money")
  private double orderDetailedOneMoney;

  @Column(name = "order_detailed_number")
  private int orderDetailedNumber;

  @Column(name = "order_state")
  private int orderState;

}
