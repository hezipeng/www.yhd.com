package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 首页楼层表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_home_floor")
public class YhdHomeFloorBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "floor_id")
  private int floorId;

  @Column(name = "floor_name")
  private String floorName;

  @Column(name = "floor_title")
  private String floorTitle;

  @Column(name = "floor_left_picture")
  private String floorLeftPicture;

}
