package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 店铺经营信息表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_shop_operate")
public class YhdShopOperateBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "shop_operate_id")
  private int shopOperateId;

  @ManyToOne
  @JoinColumn(name = "shop_id")
  private YhdShopBo yhdShopBo;

  @Column(name = "shop_operate_firm_name")
  private String shopOperateFirmName;

  @Column(name = "shop_operate_firm_phone")
  private String shopOperateFirmPhone;

  @Column(name = "shop_operate_firm_address")
  private String shopOperateFirmAddress;

  @Column(name = "shop_operate_contacts_name")
  private String shopOperateContactsName;

  @Column(name = "shop_operate_contacts_phone")
  private String shopOperateContactsPhone;

  @Column(name = "shop_operate_contacts_email")
  private String shopOperateContactsEmail;

  @Column(name = "shop_operate_firm_offcial_address")
  private String shopOperateFirmOffcialAddress;

  @Column(name = "shop_operate_whether_retailers")
  private int shopOperateWhetherRetailers;

  @Column(name = "shop_operate_operate_number")
  private int shopOperateOperateNumber;

  @Column(name = "shop_operate_depot")
  private int shopOperateDepot;

  @Column(name = "shop_operate_depot_address")
  private String shopOperateDepotAddress;

}
