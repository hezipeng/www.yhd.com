package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 用户个人资料表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_user_material")
public class YhdUserMaterialBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "material_id")
  private int materialId;

  @ManyToOne
  @JoinColumn(name = "user_id")
  private YhdUserBo yhdUserBo;

  @Column(name = "material_portrait")
  private String materialPortrait;

  @Column(name = "material_name")
  private String materialName;

  @Column(name = "material_sex")
  private String materialSex;

  @Column(name = "material_birthday")
  private String materialBirthday;

}
