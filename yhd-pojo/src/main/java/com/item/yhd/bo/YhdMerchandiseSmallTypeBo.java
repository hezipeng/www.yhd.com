package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 产品小类型表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_merchandise_small_type")
public class YhdMerchandiseSmallTypeBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "small_type_id")
  private int smallTypeId;

  @ManyToOne
  @JoinColumn(name = "large_type_id")
  private YhdMerchandiseLargeTypeBo yhdMerchandiseLargeTypeBo;

  @Column(name = "small_type_name")
  private String smallTypeName;

}
