package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 物流公司表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_logistics")
public class YhdLogisticsBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "logistics_id")
  private int logisticsId;

  @Column(name = "logistics_name")
  private String logisticsName;

  @Column(name = "logistics_whether_show")
  private int logisticsWhetherShow;

}
