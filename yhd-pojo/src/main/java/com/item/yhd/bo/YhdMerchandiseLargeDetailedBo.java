package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 大产品详细表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_merchandise_large_detailed")
public class YhdMerchandiseLargeDetailedBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "large_detailed_id")
  private int largeDetailedId;

  @ManyToOne
  @JoinColumn(name = "shop_id")
  private YhdShopBo yhdShopBo;

  @ManyToOne
  @JoinColumn(name = "small_type_id")
  private YhdMerchandiseSmallTypeBo yhdMerchandiseSmallTypeBo;

  @ManyToOne
  @JoinColumn(name = "merchandise_label_id")
  private YhdMerchandiseLabelBo yhdMerchandiseLabelBo;

  @ManyToOne
  @JoinColumn(name = "brand_id")
  private YhdBrandBo yhdBrandBo;

  @Column(name = "large_detailed_gross")
  private String largeDetailedGross;

  @Column(name = "large_detailed_place")
  private String largeDetailedPlace;

  @Column(name = "large_detailed_whether_lower")
  private int largeDetailedWhetherLower;

}
