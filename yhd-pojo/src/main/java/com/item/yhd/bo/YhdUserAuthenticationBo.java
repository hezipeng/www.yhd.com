package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 用户实名认证表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_user_authentication")
public class YhdUserAuthenticationBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "authentication_id")
  private int authenticationId;

  @ManyToOne
  @JoinColumn(name = "user_id")
  private YhdUserBo yhdUserBo;

  @Column(name = "authentication_name")
  private String authenticationName;

  @Column(name = "authentication_type")
  private String authenticationType;

  @Column(name = "authentication_number")
  private String authenticationNumber;

  @Column(name = "authentication_credit_type")
  private String authenticationCreditType;

  @Column(name = "authentication_credit_number")
  private String authenticationCreditNumber;

  @Column(name = "authentication_credit_phone")
  private String authenticationCreditPhone;

}
