package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 用户评价图片表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_user_picture_comment")
public class YhdUserPictureCommentBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "picture_comment_id")
  private int pictureCommentId;

  @ManyToOne
  @JoinColumn(name = "comment_id")
  private YhdUserMerchandiseCommentBo yhdUserMerchandiseCommentBo;

  @Column(name = "comment_picture")
  private String commentPicture;

}
