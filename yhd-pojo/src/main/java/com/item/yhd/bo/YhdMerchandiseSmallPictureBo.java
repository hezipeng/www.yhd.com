package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 小产品图片表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_merchandise_small_picture")
public class YhdMerchandiseSmallPictureBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "picture_id")
  private int pictureId;

  @ManyToOne
  @JoinColumn(name = "small_detailed_id")
  private YhdMerchandiseSmallDetailedBo yhdMerchandiseSmallDetailedBo;

  @Column(name = "picture_address")
  private String pictureAddress;

  @Column(name = "picture_whether_show")
  private int pictureWhetherShow;

}
