package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 首页楼层产品列表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_home_floor_merchandise")
public class YhdHomeFloorMerchandiseBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "floor_merchandise_id")
  private int floorMerchandiseId;

  @ManyToOne
  @JoinColumn(name = "floor_id")
  private YhdHomeFloorBo yhdHomeFloorBo;

  @ManyToOne
  @JoinColumn(name = "large_detailed_id")
  private YhdMerchandiseLargeDetailedBo yhdMerchandiseLargeDetailedBo;

  @ManyToOne
  @JoinColumn(name = "small_detailed_id")
  private YhdMerchandiseSmallDetailedBo yhdMerchandiseSmallDetailedBo;

}
