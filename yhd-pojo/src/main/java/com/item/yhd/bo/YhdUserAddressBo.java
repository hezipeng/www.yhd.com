package com.item.yhd.bo;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;

/**
 * 用户收货地址
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_user_address")
@Proxy(lazy = false)
public class YhdUserAddressBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "address_id")
  private int addressId;

  @ManyToOne
  @JoinColumn(name = "user_id")
  @JsonIgnore
  private YhdUserBo yhdUserBo;

  @Column(name = "address_name")
  private String addressName;

  @Column(name = "address_goods")
  private String addressGoods;

  @Column(name = "address_detailed")
  private String addressDetailed;

  @Column(name = "address_phone")
  private String addressPhone;

  @Column(name = "address_label")
  private String addressLabel;

  @Column(name = "address_whether_default")
  private int addressWhetherDefault;

}
