package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 店铺等级表
 */
@Getter
@Setter
@Entity
  @Table(name = "yhd_shop_grade")
public class YhdShopGradeBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "shop_grade_id")
  private int shopGradeId;

  @Column(name = "shop_grade_name")
  private String shopGradeName;

  @Column(name = "shop_grade_picture")
  private String shopGradePicture;

  @Column(name = "shop_grade_number")
  private int shopGradeNumber;

  @Column(name = "shop_grade_money")
  private double shopGradeMoney;

}
