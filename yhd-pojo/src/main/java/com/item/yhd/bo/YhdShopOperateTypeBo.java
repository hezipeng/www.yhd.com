package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 店铺可经营类型表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_shop_operate_type")
public class YhdShopOperateTypeBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "operate_type_id")
  private int operateTypeId;

  @ManyToOne
  @JoinColumn(name = "shop_id")
  private YhdShopBo yhdShopBo;

  @ManyToOne
  @JoinColumn(name = "large_type_id")
  private YhdMerchandiseLargeTypeBo yhdMerchandiseLargeTypeBo;

}
