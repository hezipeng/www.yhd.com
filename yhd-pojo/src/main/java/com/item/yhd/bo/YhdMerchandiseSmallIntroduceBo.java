package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 小产品介绍表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_merchandise_small_introduce")
public class YhdMerchandiseSmallIntroduceBo {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "introduce_id")
  private int introduceId;

  @ManyToOne
  @JoinColumn(name = "small_detailed_id")
  private YhdMerchandiseSmallDetailedBo yhdMerchandiseSmallDetailedBo;

  @Column(name = "introduce_picture")
  private String introducePicture;

}
