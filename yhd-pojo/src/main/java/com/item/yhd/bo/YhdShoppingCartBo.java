package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 购物车表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_shopping_cart")
public class YhdShoppingCartBo implements java.io.Serializable {

  @Id
  private int id;

  @ManyToOne
  @JoinColumn(name = "user_id")
  private YhdUserBo yhdUserBo;

  @ManyToOne
  @JoinColumn(name = "large_detailed_id")
  private YhdMerchandiseLargeDetailedBo yhdMerchandiseLargeDetailedBo;

  @ManyToOne
  @JoinColumn(name = "small_detailed_id")
  private YhdMerchandiseSmallDetailedBo yhdMerchandiseSmallDetailedBo;

  @Column(name = "shopping_cart_price")
  private double shoppingCartPrice;

  @Column(name = "shopping_cart_number")
  private int shoppingCartNumber;

}
