package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 用户关注的商品表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_user_merchandise_follow")
public class YhdUserMerchandiseFollowBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "follow_id")
  private int followId;

  @ManyToOne
  @JoinColumn(name = "user_id")
  private YhdUserBo yhdUserBo;

  @ManyToOne
  @JoinColumn(name = "large_detailed_id")
  private YhdMerchandiseLargeDetailedBo yhdMerchandiseLargeDetailedBo;

  @ManyToOne
  @JoinColumn(name = "small_detailed_id")
  private YhdMerchandiseSmallIntroduceBo yhdMerchandiseSmallIntroduceBo;

}
