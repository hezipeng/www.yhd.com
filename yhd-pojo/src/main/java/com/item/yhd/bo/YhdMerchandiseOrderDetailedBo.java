package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 产品订单详细表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_merchandise_order_detailed")
public class YhdMerchandiseOrderDetailedBo implements java.io.Serializable {

  @Id
  private int id;

  @ManyToOne
  @JoinColumn(name = "order_id")
  private YhdMerchandiseOrderBo yhdMerchandiseOrderBo;

  @ManyToOne
  @JoinColumn(name = "large_detailed_id")
  private YhdMerchandiseLargeDetailedBo yhdMerchandiseLargeDetailedBo;

  @ManyToOne
  @JoinColumn(name = "small_detailed_id")
  private YhdMerchandiseSmallDetailedBo yhdMerchandiseSmallDetailedBo;

  @Column(name = "order_detailed_one_money")
  private double orderDetailedOneMoney;

  @Column(name = "order_detailed_number")
  private int orderDetailedNumber;

  @Column(name = "order_state")
  private int orderState;

}
