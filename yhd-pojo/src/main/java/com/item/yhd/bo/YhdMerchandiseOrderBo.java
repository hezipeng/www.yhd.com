package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 产品订单表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_merchandise_order")
public class YhdMerchandiseOrderBo implements java.io.Serializable {

  @Id
  @Column(name = "order_id")
  private String orderId;

  @ManyToOne
  @JoinColumn(name = "user_id")
  private YhdUserBo yhdUserBo;

  @Column(name = "order_pay_mode")
  private String orderPayMode;

  @Column(name = "order_delivery_mode")
  private String orderDeliveryMode;

  @Column(name = "order_consignee")
  private String orderConsignee;

  @Column(name = "order_phone")
  private String orderPhone;

  @Column(name = "order_area")
  private String orderArea;

  @Column(name = "order_address")
  private String orderAddress;

  @Column(name = "order_address_alias")
  private String orderAddressAlias;

  @Column(name = "order_sum_money")
  private double orderSumMoney;

  @Column(name = "order_datetime")
  private String orderDatetime;

  @Column(name = "order_state")
  private int orderState;

}
