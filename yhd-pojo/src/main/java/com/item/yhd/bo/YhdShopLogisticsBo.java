package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 店铺物流表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_shop_logistics")
public class YhdShopLogisticsBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "shop_logistics_id")
  private int shopLogisticsId;

  @ManyToOne
  @JoinColumn(name = "shop_id")
  private YhdShopBo yhdShopBo;

  @ManyToOne
  @JoinColumn(name = "logistics_id")
  private YhdLogisticsBo yhdLogistics;

  @Column(name = "logistics_whether_show")
  private int logisticsWhetherShow;

}
