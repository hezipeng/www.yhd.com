package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 产品类型组
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_merchandise_type_group")
public class YhdMerchandiseTypeGroupBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "group_id")
  private int groupId;

  @Column(name = "group_name")
  private String groupName;

  @ManyToOne
  @JoinColumn(name = "large_type_id")
  private YhdMerchandiseLargeTypeBo yhdMerchandiseLargeTypeBo;

}
