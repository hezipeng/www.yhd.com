package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 限时抢购产品详细表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_flash_sale_merchandise")
public class YhdFlashSaleMerchandiseBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "sale_merchandise_id")
  private int saleMerchandiseId;

  @ManyToOne
  @JoinColumn(name = "sale_id")
  private YhdFlashSaleBo yhdFlashSaleBo;

  @ManyToOne
  @JoinColumn(name = "shop_id")
  private YhdShopBo yhdShopBo;

  @ManyToOne
  @JoinColumn(name = "large_detailed_id")
  private YhdMerchandiseLargeDetailedBo yhdMerchandiseLargeDetailedBo;

  @ManyToOne
  @JoinColumn(name = "small_detailed_id")
  private YhdMerchandiseSmallDetailedBo yhdMerchandiseSmallDetailedBo;

}
