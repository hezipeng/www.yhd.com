package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 首页楼层主标题分类表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_home_floor_title")
public class YhdHomeFloorTitleBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "floor_title_id")
  private int floorTitleId;

  @ManyToOne
  @JoinColumn(name = "floor_id")
  private YhdHomeFloorBo yhdHomeFloorBo;

  @ManyToOne
  @JoinColumn(name = "small_type_id")
  private YhdMerchandiseSmallTypeBo yhdMerchandiseSmallTypeBo;

}
