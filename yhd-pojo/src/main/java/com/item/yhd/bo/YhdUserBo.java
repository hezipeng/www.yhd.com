package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * 用户表
 */
@Getter
@Setter
@ToString
@Entity
@Table(name = "yhd_user")
@Proxy(lazy = false)
public class YhdUserBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "user_id")
  private int userId;

  @Column(name = "user_account")
  private String userAccount;

  @Column(name = "user_password")
  private String userPassword;

  @Column(name = "user_datetime")
  private String userDatetime;

  @Column(name = "user_phone")
  private String userPhone;

  @Column(name = "user_email")
  private String userEmail;

  @OneToMany(mappedBy = "yhdUserBo",fetch = FetchType.EAGER) //立即加载
  private Set<YhdUserAddressBo> sets = new HashSet<YhdUserAddressBo>();

}
