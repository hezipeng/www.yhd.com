package com.item.yhd.bo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * 产品大类型表
 */
@Getter
@Setter
@Entity
@Table(name = "yhd_merchandise_large_type")
public class YhdMerchandiseLargeTypeBo implements java.io.Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "large_type_id")
  private int largeTypeId;

  @Column(name = "large_type_name")
  private String largeTypeName;

}
