package com.item.yhd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YhdItemUserProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(YhdItemUserProviderApplication.class, args);
    }

}
