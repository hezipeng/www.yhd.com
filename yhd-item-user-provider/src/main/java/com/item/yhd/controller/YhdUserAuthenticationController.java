package com.item.yhd.controller;

import com.item.yhd.po.YhdUserAuthentication;
import com.item.yhd.service.YhdUserAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/yhdUserAuthenticationController")
public class YhdUserAuthenticationController {

    @Autowired
    private YhdUserAuthenticationService yhdUserAuthenticationService;


    @RequestMapping(value = "/getShowYhdUserAuthentication/{authenticationId}", method = RequestMethod.GET)
    public YhdUserAuthentication getShowYhdUserAuthentication(@PathVariable("authenticationId") int authenticationId) {
        return this.yhdUserAuthenticationService.getYhdUserAuthenticationService(authenticationId);
    }

    @RequestMapping(value = "/addInsertYhdUserAuthentication", method = RequestMethod.GET)
    public YhdUserAuthentication addInsertYhdUserAuthentication(YhdUserAuthentication yhdUserAuthentication) {
        return this.yhdUserAuthenticationService.addYhdUserAuthenticationService(yhdUserAuthentication);
    }

    @RequestMapping(value = "/setUpdateYhdUserAuthentication", method = RequestMethod.GET)
    public YhdUserAuthentication setUpdateYhdUserAuthentication(YhdUserAuthentication yhdUserAuthentication) {
        return this.yhdUserAuthenticationService.setYhdUserAuthenticationService(yhdUserAuthentication);
    }

    @RequestMapping(value = "/setUpdateYhdUserAuthentication/{authenticationId}", method = RequestMethod.GET)
    public boolean delDeleteYhdUserAuthentication(@PathVariable("authenticationId") int authenticationId) {
        return this.yhdUserAuthenticationService.delYhdUserAuthenticationService(authenticationId);
    }

}
