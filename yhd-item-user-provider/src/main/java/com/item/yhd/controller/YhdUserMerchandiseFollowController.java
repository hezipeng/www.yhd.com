package com.item.yhd.controller;

import com.item.yhd.po.YhdUserMerchandiseFollow;
import com.item.yhd.service.YhdUserMerchandiseFollowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/yhdUserMerchandiseFollowController")
public class YhdUserMerchandiseFollowController {

    @Autowired
    private YhdUserMerchandiseFollowService yhdUserMerchandiseFollowService;


    @RequestMapping(value = "/getShowListYhdUserMerchandiseFollow/{userId}", method = RequestMethod.GET)
    public List<YhdUserMerchandiseFollow> getShowListYhdUserMerchandiseFollow(@PathVariable("userId") int userId) {
        return this.yhdUserMerchandiseFollowService.getListYhdUserMerchandiseFollowService(userId);
    }

    @RequestMapping(value = "/addInsertYhdUserMerchandiseFollow", method = RequestMethod.GET)
    public YhdUserMerchandiseFollow addInsertYhdUserMerchandiseFollow(YhdUserMerchandiseFollow yhdUserMerchandiseFollow) {
        return this.yhdUserMerchandiseFollowService.addYhdUserMerchandiseFollowService(yhdUserMerchandiseFollow);
    }

    @RequestMapping(value = "/setUpdateYhdUserMerchandiseFollow", method = RequestMethod.GET)
    public YhdUserMerchandiseFollow setUpdateYhdUserMerchandiseFollow(YhdUserMerchandiseFollow yhdUserMerchandiseFollow) {
        return this.yhdUserMerchandiseFollowService.setYhdUserMerchandiseFollowService(yhdUserMerchandiseFollow);
    }

    @RequestMapping(value = "/delDeleteYhdUserMerchandiseFollow/{followId}", method = RequestMethod.GET)
    public boolean delDeleteYhdUserMerchandiseFollow(@PathVariable("followId") int followId) {
        try {
            this.yhdUserMerchandiseFollowService.delYhdUserMerchandiseFollowService(followId);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
