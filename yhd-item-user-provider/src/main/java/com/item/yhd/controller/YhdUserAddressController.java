package com.item.yhd.controller;

import com.item.yhd.po.YhdUserAddress;
import com.item.yhd.service.YhdUserAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/yhdUserAddressController")
public class YhdUserAddressController {

    @Autowired
    private YhdUserAddressService yhdUserAddressService;


    @RequestMapping(value = "/getSelectYhdUserAddressPageMap/{page}/{size}", method = RequestMethod.GET)
    public Map<String, Object> getSelectYhdUserAddressPageMap(@PathVariable("page") int page, @PathVariable("size") int size) {
        return this.yhdUserAddressService.getYhdUserAddressPage(page, size);
    }

    @RequestMapping(value = "/getSelectYhdUserAddressList/{userId}", method = RequestMethod.GET)
    public List<YhdUserAddress> getSelectYhdUserAddressList(@PathVariable("userId") int userId) {
        return this.yhdUserAddressService.getYhdUserAddressList(userId);
    }

    @RequestMapping(value = "/getSelectYhdUserAddressOne/{addressId}", method = RequestMethod.GET)
    public YhdUserAddress getSelectYhdUserAddressOne(@PathVariable("addressId") int addressId) {
        return this.yhdUserAddressService.getYhdUserAddressOne(addressId);
    }

    @RequestMapping(value = "/addInsertYhdUserAddressOne", method = RequestMethod.GET)
    public YhdUserAddress addInsertYhdUserAddressOne(YhdUserAddress yhdUserAddress) {
        return this.yhdUserAddressService.addYhdUserAddressOne(yhdUserAddress);
    }

    @RequestMapping(value = "/setUpdateYhdUserAddressOne", method = RequestMethod.GET)
    public YhdUserAddress setUpdateYhdUserAddressOne(YhdUserAddress yhdUserAddress) {
        return this.yhdUserAddressService.setYhdUserAddressOne(yhdUserAddress);
    }

    @RequestMapping(value = "/delDeleteYhdUserAddressOne/{addressId}", method = RequestMethod.GET)
    public boolean delDeleteYhdUserAddressOne(@PathVariable("addressId") int addressId) {
        return this.yhdUserAddressService.delYhdUserAddressOne(addressId);
    }

}
