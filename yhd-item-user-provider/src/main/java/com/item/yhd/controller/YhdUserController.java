package com.item.yhd.controller;

import com.item.yhd.po.YhdUser;
import com.item.yhd.service.YhdUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/yhdUserController")
public class YhdUserController {

    @Autowired
    private YhdUserService yhdUserService;


    @RequestMapping(value = "/getPageMap/{page}/{size}", method = RequestMethod.GET)
    public Map<String, Object> getPageMap(@PathVariable("page") int page, @PathVariable("size") int size) {
        return this.yhdUserService.getYhdUserPage(page, size);
    }

    @RequestMapping(value = "/getOneByUserId/{userId}", method = RequestMethod.GET)
    public YhdUser getOneByUserId(@PathVariable("userId") int userId) {
        return this.yhdUserService.getYhdUserOne(userId);
    }

    @RequestMapping(value = "/addInsertYhdUser", method = RequestMethod.GET)
    public YhdUser addInsertYhdUser(YhdUser yhdUser) {
        return this.yhdUserService.addYhdUserOne(yhdUser);
    }

    @RequestMapping(value = "/setUpdateYhdUser", method = RequestMethod.GET)
    public YhdUser setUpdateYhdUser(YhdUser yhdUser) {
        return this.yhdUserService.setYhdUserOne(yhdUser);
    }

    @RequestMapping(value = "/setDeleteYhdUser/{userId}", method = RequestMethod.GET)
    public boolean setDeleteYhdUser(@PathVariable("userId") int userId) {
        return this.yhdUserService.delYhdUserOne(userId);
    }

}
