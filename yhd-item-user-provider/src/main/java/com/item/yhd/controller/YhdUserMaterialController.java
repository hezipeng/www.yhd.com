package com.item.yhd.controller;

import com.item.yhd.po.YhdUserMaterial;
import com.item.yhd.service.YhdUserMaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/yhdUserMaterialController")
public class YhdUserMaterialController {

    @Autowired
    private YhdUserMaterialService yhdUserMaterialService;


    @RequestMapping(value = "/getShowYhdUserMaterial/{userId}", method = RequestMethod.GET)
    public YhdUserMaterial getShowYhdUserMaterial(@PathVariable("userId") int userId) {
        return this.yhdUserMaterialService.getOneYhdUserMaterialService(userId);
    }

    @RequestMapping(value = "/addInsertYhdUserMaterial", method = RequestMethod.GET)
    public YhdUserMaterial addInsertYhdUserMaterial(YhdUserMaterial yhdUserMaterial) {
        return this.yhdUserMaterialService.addYhdUserMaterialService(yhdUserMaterial);
    }

    @RequestMapping(value = "/setUpdateYhdUserMaterial", method = RequestMethod.GET)
    public YhdUserMaterial setUpdateYhdUserMaterial(YhdUserMaterial yhdUserMaterial) {
        return this.yhdUserMaterialService.setYhdUserMaterialService(yhdUserMaterial);
    }

    @RequestMapping(value = "/delDeleteYhdUserMaterial/{materialId}", method = RequestMethod.GET)
    public boolean delDeleteYhdUserMaterial(@PathVariable("materialId") int materialId) {
        return this.yhdUserMaterialService.delYhdUserMaterialService(materialId);
    }

}
