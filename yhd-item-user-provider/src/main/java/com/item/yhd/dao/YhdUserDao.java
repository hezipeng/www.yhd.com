package com.item.yhd.dao;

import com.item.yhd.po.YhdUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository("yhdUserDao")
public interface YhdUserDao extends JpaRepository<YhdUser, Integer>, JpaSpecificationExecutor<YhdUser> {

}
