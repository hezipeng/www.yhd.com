package com.item.yhd.dao;

import com.item.yhd.po.YhdUserMaterial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository("yhdUserMaterialDao")
public interface YhdUserMaterialDao extends JpaRepository<YhdUserMaterial, Integer>, JpaSpecificationExecutor<YhdUserMaterial> {

    public YhdUserMaterial findByUserId(int userId);

}
