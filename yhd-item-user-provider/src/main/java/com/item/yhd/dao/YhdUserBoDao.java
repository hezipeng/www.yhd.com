package com.item.yhd.dao;

import com.item.yhd.bo.YhdUserBo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository("yhdUserBoDao")
public interface YhdUserBoDao extends JpaRepository<YhdUserBo, Integer>, JpaSpecificationExecutor<YhdUserBo> {

    public YhdUserBo findByUserId(int userId);

}
