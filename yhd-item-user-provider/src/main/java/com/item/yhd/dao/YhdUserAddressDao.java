package com.item.yhd.dao;

import com.item.yhd.po.YhdUserAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository("yhdUserAddressDao")
public interface YhdUserAddressDao extends JpaRepository<YhdUserAddress, Integer>, JpaSpecificationExecutor<YhdUserAddress> {

}
