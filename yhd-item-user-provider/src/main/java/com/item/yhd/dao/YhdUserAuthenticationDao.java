package com.item.yhd.dao;

import com.item.yhd.po.YhdUserAuthentication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository("yhdUserAuthenticationDao")
public interface YhdUserAuthenticationDao extends JpaRepository<YhdUserAuthentication, Integer>, JpaSpecificationExecutor<YhdUserAuthentication> {

    public YhdUserAuthentication findByUserId(int userId);

}
