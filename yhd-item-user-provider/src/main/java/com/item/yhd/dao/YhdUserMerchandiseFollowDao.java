package com.item.yhd.dao;

import com.item.yhd.po.YhdUserMerchandiseFollow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository("yhdUserMerchandiseFollowDao")
public interface YhdUserMerchandiseFollowDao extends JpaRepository<YhdUserMerchandiseFollow, Integer>, JpaSpecificationExecutor<YhdUserMerchandiseFollow> {

}
