package com.item.yhd.service.impl;

import com.item.yhd.dao.YhdUserMerchandiseFollowDao;
import com.item.yhd.po.YhdUserMerchandiseFollow;
import com.item.yhd.service.YhdUserMerchandiseFollowService;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Service("yhdUserMerchandiseFollowServiceImpl")
public class YhdUserMerchandiseFollowServiceImpl implements YhdUserMerchandiseFollowService {

    @Resource(name = "yhdUserMerchandiseFollowDao")
    private YhdUserMerchandiseFollowDao yhdUserMerchandiseFollowDao;


    @Override
    public List<YhdUserMerchandiseFollow> getListYhdUserMerchandiseFollowService(int userId) {
        Specification<YhdUserMerchandiseFollow> spec = new Specification<YhdUserMerchandiseFollow>() {
            @Override
            public Predicate toPredicate(Root<YhdUserMerchandiseFollow> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("userId"), userId);
            }
        };
        List<YhdUserMerchandiseFollow> list = this.yhdUserMerchandiseFollowDao.findAll(spec);
        return list;
    }

    @Override
    public YhdUserMerchandiseFollow addYhdUserMerchandiseFollowService(YhdUserMerchandiseFollow yhdUserMerchandiseFollow) {
        return this.yhdUserMerchandiseFollowDao.save(yhdUserMerchandiseFollow);
    }

    @Override
    public YhdUserMerchandiseFollow setYhdUserMerchandiseFollowService(YhdUserMerchandiseFollow yhdUserMerchandiseFollow) {
        return this.yhdUserMerchandiseFollowDao.saveAndFlush(yhdUserMerchandiseFollow);
    }

    @Override
    public boolean delYhdUserMerchandiseFollowService(int followId) {
        try {
            this.yhdUserMerchandiseFollowDao.delete(followId);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
