package com.item.yhd.service.impl;

import com.item.yhd.dao.YhdUserMaterialDao;
import com.item.yhd.po.YhdUserMaterial;
import com.item.yhd.service.YhdUserMaterialService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("yhdUserMaterialServiceImpl")
public class YhdUserMaterialServiceImpl implements YhdUserMaterialService {

    @Resource(name = "yhdUserMaterialDao")
    private YhdUserMaterialDao yhdUserMaterialDao;


    @Override
    public YhdUserMaterial getOneYhdUserMaterialService(int userId) {
        return this.yhdUserMaterialDao.findByUserId(userId);
    }

    @Override
    public YhdUserMaterial addYhdUserMaterialService(YhdUserMaterial yhdUserMaterial) {
        return this.yhdUserMaterialDao.save(yhdUserMaterial);
    }

    @Override
    public YhdUserMaterial setYhdUserMaterialService(YhdUserMaterial yhdUserMaterial) {
        return this.yhdUserMaterialDao.saveAndFlush(yhdUserMaterial);
    }

    @Override
    public boolean delYhdUserMaterialService(int materialId) {
        try {
            this.yhdUserMaterialDao.delete(materialId);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
