package com.item.yhd.service.impl;

import com.item.yhd.dao.YhdUserDao;
import com.item.yhd.po.YhdUser;
import com.item.yhd.service.YhdUserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.Map;

@Service("yhdUserServiceImpl")
public class YhdUserServiceImpl implements YhdUserService {

    @Resource(name = "yhdUserDao")
    private YhdUserDao yhdUserDao;


    @Override
    public Map<String, Object> getYhdUserPage(int page, int size) {
        Map<String, Object> map = new HashMap<String, Object>();

        // 条件
        Specification spec = new Specification() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return null;
            }
        };

        // 分页
        Pageable pageable = new PageRequest(page, size);

        Page<YhdUser> pages = this.yhdUserDao.findAll(spec, pageable);

        map.put("getTotal", pages.getTotalElements());
        map.put("getPages", pages.getTotalPages());
        map.put("list", pages.getContent());

        return map;
    }

    @Override
    public YhdUser getYhdUserOne(int userId) {
        return this.yhdUserDao.findOne(userId);
    }

    @Override
    public YhdUser addYhdUserOne(YhdUser yhdUser) {
        return this.yhdUserDao.save(yhdUser);
    }

    @Override
    public YhdUser setYhdUserOne(YhdUser yhdUser) {
        return this.yhdUserDao.saveAndFlush(yhdUser);
    }

    @Override
    public boolean delYhdUserOne(int userId) {
        try {
            this.yhdUserDao.delete(userId);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
