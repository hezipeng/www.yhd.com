package com.item.yhd.service;

import com.item.yhd.po.YhdUserAddress;

import java.util.List;
import java.util.Map;

public interface YhdUserAddressService {

    /**
     * 分页
     * @param page
     * @param size
     * @return
     */
    public Map<String, Object> getYhdUserAddressPage(int page, int size);

    /**
     * 获得用户的多个地址
     * @param userId
     * @return
     */
    public List<YhdUserAddress> getYhdUserAddressList(int userId);

    /**
     * 获得一个地址
     * @param addressId
     * @return
     */
    public YhdUserAddress getYhdUserAddressOne(int addressId);

    /**
     * 添加
     * @param yhdUserAddress
     * @return
     */
    public YhdUserAddress addYhdUserAddressOne(YhdUserAddress yhdUserAddress);

    /**
     * 修改
     * @param yhdUserAddress
     * @return
     */
    public YhdUserAddress setYhdUserAddressOne(YhdUserAddress yhdUserAddress);

    /**
     * 删除
     * @param addressId
     * @return
     */
    public boolean delYhdUserAddressOne(int addressId);

}
