package com.item.yhd.service;

import com.item.yhd.po.YhdUserMerchandiseFollow;

import java.util.List;

public interface YhdUserMerchandiseFollowService {

    /**
     * 用户关注的商品
     * @param userId
     * @return
     */
    public List<YhdUserMerchandiseFollow> getListYhdUserMerchandiseFollowService(int userId);

    /**
     * 添加一个商品关注
     * @param yhdUserMerchandiseFollow
     * @return
     */
    public YhdUserMerchandiseFollow addYhdUserMerchandiseFollowService(YhdUserMerchandiseFollow yhdUserMerchandiseFollow);

    /**
     * 修改一个商品关注
     * @param yhdUserMerchandiseFollow
     * @return
     */
    public YhdUserMerchandiseFollow setYhdUserMerchandiseFollowService(YhdUserMerchandiseFollow yhdUserMerchandiseFollow);

    /**
     * 删除
     * @param followId
     * @return
     */
    public boolean delYhdUserMerchandiseFollowService(int followId);

}
