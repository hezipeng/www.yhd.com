package com.item.yhd.service.impl;

import com.item.yhd.dao.YhdUserAuthenticationDao;
import com.item.yhd.po.YhdUserAuthentication;
import com.item.yhd.service.YhdUserAuthenticationService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("yhdUserAuthenticationServiceImpl")
public class YhdUserAuthenticationServiceImpl implements YhdUserAuthenticationService {

    @Resource(name = "yhdUserAuthenticationDao")
    private YhdUserAuthenticationDao yhdUserAuthenticationDao;


    @Override
    public YhdUserAuthentication getYhdUserAuthenticationService(int userId) {
        return this.yhdUserAuthenticationDao.findByUserId(userId);
    }

    @Override
    public YhdUserAuthentication addYhdUserAuthenticationService(YhdUserAuthentication yhdUserAuthentication) {
        return this.yhdUserAuthenticationDao.save(yhdUserAuthentication);
    }

    @Override
    public YhdUserAuthentication setYhdUserAuthenticationService(YhdUserAuthentication yhdUserAuthentication) {
        return this.yhdUserAuthenticationDao.saveAndFlush(yhdUserAuthentication);
    }

    @Override
    public boolean delYhdUserAuthenticationService(int authenticationId) {
        try {
            this.yhdUserAuthenticationDao.delete(authenticationId);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
