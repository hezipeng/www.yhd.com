package com.item.yhd.service.impl;

import com.item.yhd.dao.YhdUserAddressDao;
import com.item.yhd.po.YhdUserAddress;
import com.item.yhd.service.YhdUserAddressService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("yhdUserAddressServiceImpl")
public class YhdUserAddressServiceImpl implements YhdUserAddressService {

    @Resource(name = "yhdUserAddressDao")
    private YhdUserAddressDao yhdUserAddressDao;


    @Override
    public Map<String, Object> getYhdUserAddressPage(int page, int size) {
        Map<String, Object> map = new HashMap<String, Object>();
        // 条件
        Specification<YhdUserAddress> spec = new Specification<YhdUserAddress>() {
            @Override
            public Predicate toPredicate(Root<YhdUserAddress> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return null;
            }
        };

        // page
        Pageable pageable = new PageRequest(page, size);

        Page<YhdUserAddress> pages = this.yhdUserAddressDao.findAll(spec, pageable);

        map.put("getTotal", pages.getTotalElements());
        map.put("getPages", pages.getTotalPages());
        map.put("list", pages.getContent());

        return map;
    }

    @Override
    public List<YhdUserAddress> getYhdUserAddressList(int userId) {
        Specification<YhdUserAddress> spec = new Specification<YhdUserAddress>() {
            @Override
            public Predicate toPredicate(Root<YhdUserAddress> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("userId"), userId);
            }
        };
        return this.yhdUserAddressDao.findAll(spec);
    }

    @Override
    public YhdUserAddress getYhdUserAddressOne(int addressId) {
        return this.yhdUserAddressDao.findOne(addressId);
    }

    @Override
    public YhdUserAddress addYhdUserAddressOne(YhdUserAddress yhdUserAddress) {
        return this.yhdUserAddressDao.save(yhdUserAddress);
    }

    @Override
    public YhdUserAddress setYhdUserAddressOne(YhdUserAddress yhdUserAddress) {
        return this.yhdUserAddressDao.saveAndFlush(yhdUserAddress);
    }

    @Override
    public boolean delYhdUserAddressOne(int addressId) {
        try {
            this.yhdUserAddressDao.delete(addressId);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
