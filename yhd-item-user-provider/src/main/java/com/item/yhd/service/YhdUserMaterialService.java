package com.item.yhd.service;

import com.item.yhd.po.YhdUserMaterial;

public interface YhdUserMaterialService {

    public YhdUserMaterial getOneYhdUserMaterialService(int userId);

    public YhdUserMaterial addYhdUserMaterialService(YhdUserMaterial yhdUserMaterial);

    public YhdUserMaterial setYhdUserMaterialService(YhdUserMaterial yhdUserMaterial);

    public boolean delYhdUserMaterialService(int materialId);

}
