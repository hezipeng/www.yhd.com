package com.item.yhd.service;

import com.item.yhd.po.YhdUser;

import java.util.Map;

public interface YhdUserService {

    /**
     * 用户分页数据
     * @param page
     * @param size
     * @return
     */
    public Map<String, Object> getYhdUserPage(int page, int size);

    /**
     * 获得一个用户
     * @param userId
     * @return
     */
    public YhdUser getYhdUserOne(int userId);

    /**
     * 添加一个用户
     * @param yhdUser
     * @return
     */
    public YhdUser addYhdUserOne(YhdUser yhdUser);

    /**
     * 修改
     * @param yhdUser
     * @return
     */
    public YhdUser setYhdUserOne(YhdUser yhdUser);

    /**
     * 删除
     * @param userId
     * @return
     */
    public boolean delYhdUserOne(int userId);

}
