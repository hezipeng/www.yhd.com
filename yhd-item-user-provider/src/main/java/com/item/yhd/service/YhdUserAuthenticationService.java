package com.item.yhd.service;

import com.item.yhd.po.YhdUserAuthentication;

public interface YhdUserAuthenticationService {

    /**
     * 获得一个用户实名认证表
     * @param userId
     * @return
     */
    public YhdUserAuthentication getYhdUserAuthenticationService(int userId);

    /**
     * 添加一个用户实名认证表
     * @param yhdUserAuthentication
     * @return
     */
    public YhdUserAuthentication addYhdUserAuthenticationService(YhdUserAuthentication yhdUserAuthentication);

    /**
     * 修改一个用户实名认证表
     * @param yhdUserAuthentication
     * @return
     */
    public YhdUserAuthentication setYhdUserAuthenticationService(YhdUserAuthentication yhdUserAuthentication);

    /**
     * 删除一个用户实名认证表
     * @param authenticationId
     * @return
     */
    public boolean delYhdUserAuthenticationService(int authenticationId);

}
