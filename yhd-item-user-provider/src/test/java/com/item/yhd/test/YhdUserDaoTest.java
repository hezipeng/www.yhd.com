package com.item.yhd.test;

import com.item.yhd.bo.YhdUserBo;
import com.item.yhd.dao.YhdUserBoDao;
import com.item.yhd.dao.YhdUserDao;
import com.item.yhd.po.YhdUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class YhdUserDaoTest {

    @Resource(name = "yhdUserDao")
    private YhdUserDao yhdUserDao;
    @Resource(name = "yhdUserBoDao")
    private YhdUserBoDao yhdUserBoDao;


    @Test
    public void showUser() {
        List<YhdUserBo> list = yhdUserBoDao.findAll();

//        YhdUserBo yhdUserBo = yhdUserBoDao.getOne(1);

//        YhdUserBo yhdUserBo1 = yhdUserBoDao.findByUserId(1);

//        System.out.println(yhdUserBo1.getUserId());
//        for (YhdUserBo u : list) {
//            System.out.println(u.getUserId()+" -- "+u.getUserAccount());
//        }
    }

    @Test
    public void addUser() {
        YhdUser yhdUser = new YhdUser();
        yhdUser.setUserAccount("15511001336");
        yhdUser.setUserPassword("123456");
        yhdUser.setUserDatetime("2019-02-07");
        yhdUser.setUserEmail("15511001336@162.com");

        yhdUserDao.save(yhdUser);
        System.out.println(yhdUser.getUserId());
    }

    @Test
    public void updateUser() {
        YhdUser yhdUser = yhdUserDao.getOne(2);
        yhdUser.setUserAccount("15511001338");
        yhdUser.setUserPassword("666666");
        yhdUserDao.saveAndFlush(yhdUser);
        System.out.println(yhdUser.getUserAccount());
    }

    @Test
    public void test1() {
        Specification<YhdUserBo> spec = new Specification<YhdUserBo>() {
            @Override
            public Predicate toPredicate(Root<YhdUserBo> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
//                List<Predicate> list = new ArrayList<Predicate>();
//                list.add(criteriaBuilder.like(root.get("userAccount"),"1551100%"));
//                list.add(criteriaBuilder.like(root.get("userPassword"),"12345%"));
//                Predicate[] arr = new Predicate[list.size()];
//                return criteriaBuilder.and(list.toArray(arr));
                return null;
            }
        };

        List<YhdUserBo> list = this.yhdUserBoDao.findAll(spec);
        for (YhdUserBo y : list) {
            System.out.println(y);
        }
    }

    @Test
    public void test2() {
        // 条件
        Specification<YhdUserBo> spec = new Specification<YhdUserBo>() {
            @Override
            public Predicate toPredicate(Root<YhdUserBo> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.like(root.get("userPassword"), "1234%");
            }
        };
        // page 分页
        Pageable pageable = new PageRequest(0,2);

        Page<YhdUserBo> page = this.yhdUserBoDao.findAll(spec, pageable);

        System.out.println("中条数：" + page.getTotalElements());
        System.out.println("总页数：" + page.getTotalPages());
        List<YhdUserBo> list = page.getContent();
        for (YhdUserBo y : list) {
            System.out.println(y);
        }
    }

}
